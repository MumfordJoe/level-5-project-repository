using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using TMPro;

public class GetGameTime : MonoBehaviour
{
    private TMP_Text _textMesh;
    private GameController _GameController;

    // Start is called before the first frame update
    void Start()
    {
        _textMesh = gameObject.GetComponent<TMP_Text>();
        _GameController = GameObject.Find("GameManager").GetComponent<GameController>();

        float minutes = Mathf.Floor(_GameController.timeTaken / 60);
        string buffer = "";
        if (minutes <= 9)
        {
            buffer = "0";
        }
        float seconds = _GameController.timeTaken % 60;
        seconds = Mathf.Round(seconds);

        string text = "Time Taken: ";
        if (SceneManager.GetSceneByName("Fail Menu").isLoaded)
        {
            text = "Time Spent: ";
        }

        _textMesh.text = text + buffer + minutes.ToString() + ":" + seconds.ToString();
    }

    // Update is called once per frame
    void Update()
    {

    }
}
