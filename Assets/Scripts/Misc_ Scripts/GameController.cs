using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameController : MonoBehaviour
{
    private static GameController _gameController = null;
    public int gameLevel = 0;
    public int menuLevel = 0;
    public float timeTaken = 0;
    public int pointsObtained = 0;
    private bool playerObjectDefined = false;

    // SFX Variables
    private AudioSource _AudioSource;
    [SerializeField] private AudioClip gameOverSFX;
    [SerializeField] private AudioClip victorySFX;
    [SerializeField] private AudioClip menuMusic;
    [SerializeField] private AudioClip gameMusic;

    // Cache
    PlayerController _player;

    // Keep track of our Game's State
    public enum EGameState
    {
        MainMenu,
        Playing,
        Paused,
        GameOver,
        Victory
    }

    [SerializeField] public EGameState _eGameState; //.EGameStatePlaying

    public EGameState GetState()
    {
        return _eGameState;
    }

    public void ChangeState(EGameState eGameState)
    {
        Debug.Log("#Change State - " + eGameState);
        switch (eGameState)
        {
            ////////////////////////////////////////////////////////////////
            case EGameState.MainMenu:
                Time.timeScale = 1.0f;
                if (_eGameState == EGameState.GameOver || _eGameState == EGameState.Victory || _eGameState == EGameState.Paused)
                {
                    if (SceneManager.GetSceneByName("Level 1").isLoaded)
                    {
                        SceneManager.UnloadSceneAsync("Level 1");
                    }
                    if (SceneManager.GetSceneByName("Level 2").isLoaded)
                    {
                        SceneManager.UnloadSceneAsync("Level 2");
                    }
                    if (SceneManager.GetSceneByName("Finish Menu").isLoaded)
                    {
                        SceneManager.UnloadSceneAsync("Finish Menu");
                    }
                    if (SceneManager.GetSceneByName("Fail Menu").isLoaded)
                    {
                        SceneManager.UnloadSceneAsync("Fail Menu");
                    }
                    SceneManager.LoadSceneAsync("Main Menu", LoadSceneMode.Additive);
                    gameLevel = 0;
                    timeTaken = 0;
                    pointsObtained = 0;
                }

                _AudioSource.Stop();
                _AudioSource.loop = true;
                //_AudioSource.PlayOneShot(menuMusic, 0.0075f);
                break;
            ////////////////////////////////////////////////////////////////
            case EGameState.Playing:
                Time.timeScale = 1.0f;
                if (_eGameState == EGameState.MainMenu || _eGameState == EGameState.GameOver || _eGameState == EGameState.Victory)
                {
                    if (SceneManager.GetSceneByName("Finish Menu").isLoaded)
                    {
                        SceneManager.UnloadSceneAsync("Finish Menu");
                    }
                    if (SceneManager.GetSceneByName("Fail Menu").isLoaded)
                    {
                        SceneManager.UnloadSceneAsync("Fail Menu");
                    }
                    if (SceneManager.GetSceneByName("Main Menu").isLoaded)
                    {
                        SceneManager.UnloadSceneAsync("Main Menu");
                    }
                    if (gameLevel == 0)
                    {
                        SceneManager.LoadSceneAsync("Level 1", LoadSceneMode.Additive);
                    }
                    if (gameLevel == 1 || gameLevel == 2)
                    {
                        SceneManager.LoadSceneAsync("Level 2", LoadSceneMode.Additive);
                    }

                    _AudioSource.Stop();
                    _AudioSource.loop = true;
                    //_AudioSource.PlayOneShot(gameMusic, 0.0075f);
                }
                break;
            ////////////////////////////////////////////////////////////////
            case EGameState.GameOver:
                if (_eGameState == EGameState.Playing)
                {
                    Time.timeScale = 0.0f;

                    if (SceneManager.GetSceneByName("Level 1").isLoaded)
                    {
                        SceneManager.UnloadSceneAsync("Level 1");
                    }
                    if (SceneManager.GetSceneByName("Level 2").isLoaded)
                    {
                        SceneManager.UnloadSceneAsync("Level 2");
                    }
                    if (SceneManager.GetSceneByName("Finish Menu").isLoaded)
                    {
                        SceneManager.UnloadSceneAsync("Finish Menu");
                    }
                    if (SceneManager.GetSceneByName("Fail Menu").isLoaded)
                    {
                        SceneManager.UnloadSceneAsync("Fail Menu");
                    }
                    SceneManager.LoadSceneAsync("Fail Menu", LoadSceneMode.Additive);

                    _AudioSource.Stop();
                    _AudioSource.loop = false;
                    _AudioSource.PlayOneShot(gameOverSFX, 0.15f);
                }
                break;
            ////////////////////////////////////////////////////////////////
            case EGameState.Victory:
                if (_eGameState == EGameState.Playing)
                {
                    Time.timeScale = 0.0f;
                    if (SceneManager.GetSceneByName("Level 1").isLoaded)
                    {
                        SceneManager.UnloadSceneAsync("Level 1");
                    }
                    if (SceneManager.GetSceneByName("Level 2").isLoaded)
                    {
                        SceneManager.UnloadSceneAsync("Level 2");
                    }
                    if (SceneManager.GetSceneByName("Finish Menu").isLoaded)
                    {
                        SceneManager.UnloadSceneAsync("Finish Menu");
                    }
                    if (SceneManager.GetSceneByName("Fail Menu").isLoaded)
                    {
                        SceneManager.UnloadSceneAsync("Fail Menu");
                    }
                    SceneManager.LoadSceneAsync("Finish Menu", LoadSceneMode.Additive);

                    _AudioSource.Stop();
                    _AudioSource.loop = false;
                    _AudioSource.PlayOneShot(victorySFX, 0.15f);
                }
                break;
            ////////////////////////////////////////////////////////////////
            case EGameState.Paused:
                {
                    Time.timeScale = 0.0f;
                }
                break;
                ////////////////////////////////////////////////////////////////
                ////////////////////////////////////////////////////////////////
                ////////////////////////////////////////////////////////////////
                // The game is in an invalid state
                //default:
                //throw new ArgumentOutOfRangeException();
                ////////////////////////////////////////////////////////////////
                ////////////////////////////////////////////////////////////////
        }
        // Set our state to the new state
        _eGameState = eGameState;
    }

    void Start()
    {
        // Setting the mouse sprite to a crosshair reticle.
        _AudioSource = GetComponent<AudioSource>();
        ChangeState(EGameState.MainMenu);

        if (!SceneManager.GetSceneByName("Main Menu").isLoaded)
        {
            SceneManager.LoadSceneAsync("Main Menu", LoadSceneMode.Additive);
        }
    }

    void Awake()
    {
        // Assert we don't already have a game controller
        Debug.Assert(_gameController == null, this.gameObject);
        // Assign our static reference to this one we just created
        _gameController = this;
    }

    void Update()
    {
        if (SceneManager.GetSceneByName("Game").isLoaded == true && SceneManager.GetActiveScene() != SceneManager.GetSceneByName("Game"))
        {
            SceneManager.SetActiveScene(SceneManager.GetSceneByName("Game"));
        }


        if (playerObjectDefined == false)
        {
            if (SceneManager.GetSceneByName("Game").isLoaded == true)
            {
                _player = GameObject.Find("Player").gameObject.GetComponent<PlayerController>();
                playerObjectDefined = true;
            }
        }

        if (SceneManager.GetSceneByName("Game").isLoaded == false)
        {
            playerObjectDefined = false;
        }
        else
        {
            if (_player.health <= 0)
            {
                ChangeState(EGameState.GameOver);
            }
        }

        switch (_eGameState)
        {
            ////////////////////////////////////////////////////////////////
            // The game is at the main menu
            case EGameState.MainMenu:
                if (Input.GetKeyDown(KeyCode.Escape))
                {
                    Application.Quit();
                }
                if (!_AudioSource.isPlaying)
                {
                    _AudioSource.PlayOneShot(menuMusic, 0.0075f);
                }
                break;
            ////////////////////////////////////////////////////////////////
            // We are playing the game
            case EGameState.Playing:
                timeTaken += Time.deltaTime;
                if (Input.GetKeyDown(KeyCode.Escape))
                {
                    ChangeState(EGameState.Paused);
                }
                if (!_AudioSource.isPlaying)
                {
                    _AudioSource.PlayOneShot(gameMusic, 0.0075f);
                }
                break;
            ////////////////////////////////////////////////////////////////
            // The game is at the gameover screen
            case EGameState.GameOver:
                if (Input.GetKeyDown(KeyCode.Escape))
                {
                    ChangeState(EGameState.MainMenu);
                }
                break;
            ////////////////////////////////////////////////////////////////
            // The game is at the victory screen
            case EGameState.Victory:
                if (Input.GetKeyDown(KeyCode.Escape))
                {
                    ChangeState(EGameState.MainMenu);
                }
                break;
            ////////////////////////////////////////////////////////////////
            // Game is paused
            case EGameState.Paused:
                if (Input.GetKeyDown(KeyCode.Escape))
                {
                    ChangeState(EGameState.Playing);
                }
                break;
                ////////////////////////////////////////////////////////////////
                ////////////////////////////////////////////////////////////////
                ////////////////////////////////////////////////////////////////
                // The game is in an invalid state
                //default:
                //throw new ArgumentOutOfRangeException();
                ////////////////////////////////////////////////////////////////
                ////////////////////////////////////////////////////////////////
        }
    }
}
