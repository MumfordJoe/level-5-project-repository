using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class GameStateButtonController_Playing_ChangeText : MonoBehaviour
{
    private GameController _GameController;

    private TMP_Text _textMesh;

    // Start is called before the first frame update
    void Start()
    {
        _GameController = GameObject.Find("GameManager").GetComponent<GameController>();
        _textMesh = gameObject.GetComponent<TMP_Text>();
    }

    // Update is called once per frame
    void Update()
    {
        if (_GameController.gameLevel == 2)
        {
            _textMesh.text = "Play Again";
        }
        else
        {

            _textMesh.text = "Next Level";
        }
    }
}
