using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameStateButtonController_Menu : MonoBehaviour
{
    // Vars
    //private ColorBlock col;
    //private float time;
    private GameController _GameController;
    private GameObject _GameManager;
    [SerializeField] private GameController.EGameState SetToState = GameController.EGameState.MainMenu;
    [SerializeField] private bool pressed = false;
    // Cache
    //private Button referenceButton;




    public void OnButtonPress()
    {
        //_GameController._eGameState = SetToState;
        _GameController.ChangeState(SetToState);
        pressed = true;
        _GameController.menuLevel = 0;
    }

    // Start is called before the first frame update
    void Start()
    {
        //referenceButton = this.gameObject.GetComponent<Button>();
        //col = referenceButton.colors;
        _GameManager = GameObject.Find("GameManager");
        _GameController = _GameManager.GetComponent<GameController>();
    }

    // Update is called once per frame
    void Update()
    {
        // Code to make the 'Play' Button glow.
        //time += Time.deltaTime;
        //col.colorMultiplier = Mathf.Clamp(Mathf.Sin(time * 2) * 3, 1, 3);
        //referenceButton.colors = col;
    }
}