using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class HideObject : MonoBehaviour
{
    private GameController _GameController;
    private int menuStateNo = 0;
    [SerializeField] bool isText = false;
    [SerializeField] private int ShowForMenuStateNo = 0;
    private Vector3 pos;
    private Vector4 color;

    private TMP_Text _textMesh;

    // Start is called before the first frame update
    void Start()
    {
        _GameController = GameObject.Find("GameManager").GetComponent<GameController>();
        _textMesh = gameObject.GetComponent<TMP_Text>();
        pos = new Vector3(gameObject.transform.position.x, gameObject.transform.position.y, gameObject.transform.position.z);
        if (isText)
        {
            color = _textMesh.color;
        }
    }

    // Update is called once per frame
    void Update()
    {
        menuStateNo = _GameController.menuLevel;

        if (ShowForMenuStateNo != menuStateNo)
        {
            if (!isText)
            {
                gameObject.transform.position = pos + new Vector3(5000,0,0);
            }
            else
            {
                _textMesh.color = new Vector4(color.x, color.y, color.z, 0);
            }
        }
        else
        {
            if (!isText)
            {
                gameObject.transform.position = pos;
            }
            else
            {
                _textMesh.color = new Vector4(color.x, color.y, color.z, 255);
            }
        }
    }
}
