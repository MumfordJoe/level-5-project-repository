using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController : MonoBehaviour
{
    [SerializeField] public int limitsNumber;   //1, 2 or 3
    private Transform cameraLimits1;
    private Transform cameraLimits2;
    private Transform cameraLimits3;
    Transform _transform;

    // Start is called before the first frame update
    void Start()
    {
        cameraLimits1 = GameObject.Find("Camera Limits - First").transform;
        cameraLimits2 = GameObject.Find("Camera Limits - Second").transform;
        cameraLimits3 = GameObject.Find("Camera Limits - Third").transform;
        _transform = transform;
        limitsNumber = 1;
    }

    // Update is called once per frame
    void Update()
    {
        switch (limitsNumber)
        {
            case 1:
                {
                    _transform.position = cameraLimits1.position;
                    _transform.localScale = cameraLimits1.localScale;
                    break;
                }
            case 2:
                {
                    _transform.position = cameraLimits2.position;
                    _transform.localScale = cameraLimits2.localScale;
                    break;
                }
            case 3:
                {
                    _transform.position = cameraLimits3.position;
                    _transform.localScale = cameraLimits3.localScale;
                    break;
                }
        }
    }
}
