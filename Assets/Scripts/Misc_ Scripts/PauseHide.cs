using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class PauseHide : MonoBehaviour
{
    // Start is called before the first frame update

    private GameController _GameController;
    [SerializeField] bool isText = false;
    private Vector3 pos;
    private Vector4 color;
    Transform _transform;

    private TMP_Text _textMesh;

    // Start is called before the first frame update
    void Start()
    {
        _GameController = GameObject.Find("GameManager").GetComponent<GameController>();
        _textMesh = gameObject.GetComponent<TMP_Text>();
        pos = new Vector3(gameObject.transform.position.x, gameObject.transform.position.y, gameObject.transform.position.z);
        if (isText)
        {
            color = _textMesh.color;
        }
    }

    // Update is called once per frame
    void Update()
    {

        if (_GameController._eGameState == GameController.EGameState.Playing)
        {
            if (!isText)
            {
                gameObject.transform.position = pos + new Vector3(5000, 0, 0);
            }
            else
            {
                _textMesh.color = new Vector4(color.x, color.y, color.z, 0);
            }
        }
        else
        {
            if (!isText)
            {
                gameObject.transform.position = pos;
            }
            else
            {
                _textMesh.color = new Vector4(color.x, color.y, color.z, 255);
            }

        }
    }
}
