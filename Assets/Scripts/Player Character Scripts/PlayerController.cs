using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.InputSystem;


public class PlayerController : MonoBehaviour
{
    // Entity Values ========================
    // Current Vals
    [SerializeField] public int health;
    [NonSerialized] private float dashXTranslation;
    [NonSerialized] private float dashYTranslation;
    [NonSerialized] public bool isFacingRight = true;                   // If false then the entity is facing left.
    [NonSerialized] private float objGravity;
    [SerializeField] public bool refreshShield = false;
    [SerializeField] public bool shieldBroken = false;

    // [SerializeField] private (SpriteVar) currentSprite = (basesprite);

    // Maximum & Static Vals
    [Tooltip("The health the player starts with and the maximum the player's health can be.")] 
    [SerializeField] private int health_max = 20; 
    [SerializeField] public int shieldHealth_max = 3;
    [SerializeField] private float walkingSpeed = 8;
    //[SerializeField] private float runningSpeed = 12;
    [NonSerialized] private float dashSpeed = 1000.0f;
    [NonSerialized] private double dashLength = 0.25f;
    [SerializeField] private float jumpStrength = 12;
    [NonSerialized] private float parryLength = 0.5f;
    //[SerializeField] private float attackStrength = 1; // Change this in the player_attack object itself.
    [NonSerialized] private double airborneDrift = 0.025;
    [NonSerialized] private float hurtKnockback = 4;

    // Sprite & Animation Vals
    // stuff here

    // Player Boolean Flags
    // 'Limiter' Flags
    [NonSerialized] public bool canJump = true;                         // When false the player cannot jump, when true they can.
    [NonSerialized] private bool canMove = true;                        // When false the player cannot move, when true they can.
    [NonSerialized] private bool canAttack = true;                      // When false the player cannot attack, when true they can.
    [NonSerialized] public bool canShield = true;                      // When false the player cannot shield, when true they can.
    [NonSerialized] public bool canParry = true;                       // When false the player cannot parry, when true they can.
    [NonSerialized] private bool canDash = false;                       // When false the player cannot dash, when true they can.
    [NonSerialized] public bool showSmokeParticles = false;
    // 'Button' Flags
    [NonSerialized] private bool startAttacking = false;
    [NonSerialized] private bool startParrying = false;
    [NonSerialized] public bool startStunned = false;                   // When true: it is turned false before triggering the stunned cooldown as well as setting canMove to false until the cooldown is over.
    [NonSerialized] private bool startDashing = false;
    //public bool isAirborne = false;   // Unused
    // 'State' Flags
    [NonSerialized] public bool isShielding = false;
    [NonSerialized] public bool isParrying = false;
    [NonSerialized] public bool isDashing = false;
    [NonSerialized] public bool jumpKeyPressed = false;
    [NonSerialized] private bool attackKeyPressed = false;
    [NonSerialized] private bool isCollidingHorizontally_left = false;
    [NonSerialized] private bool isCollidingHorizontally_right = false;
    [NonSerialized] public bool isHurt = false;
    [NonSerialized] public bool isHurt_FromRight = false;
    [NonSerialized] private bool isHurt_Knockback = false;

    // Player Cooldown Vals
    // Attack Cooldown
    [SerializeField] private float attackCooldown = 0;
    [SerializeField] private float attackCooldown_length = 0.5f;            // Placeholder Value
    [NonSerialized] private bool attackCooldown_flag = false;
    // Shield Cooldown
    [NonSerialized] private float shieldCooldown = 0;
    [SerializeField] private float shieldCooldown_length = 10.0f;             // Placeholder Value
    [NonSerialized] private bool repairingShield = false;
    [NonSerialized] private bool shieldCooldown_flag = false;
    // Parry
    [NonSerialized] private float parryTimer = 0;
    [NonSerialized] private float parryTimer_length;
    [NonSerialized] private bool parryTimer_flag = false;
    // Parry Cooldown
    [NonSerialized] private float parryCooldown = 0;
    [SerializeField] private float parryCooldown_length = 4.0f;               // Placeholder Value
    [NonSerialized] private bool parryCooldown_flag = false;
    // Stunned Cooldown
    [NonSerialized] private float stunnedCooldown = 0;
    [SerializeField] private float stunnedCooldown_length = 1.0f;            // Placeholder Value
    [NonSerialized] private bool stunnedCooldown_flag = false;
    // Dash
    [NonSerialized] public double dashTimer = 0;
    [NonSerialized] private double dashTimer_length;
    [NonSerialized] private bool dashTimer_flag = false;

    // Player Control/Input
    GameInput _playerInput;
    [NonSerialized] private bool input_Jump;
    [NonSerialized] private bool input_Dash;
    [NonSerialized] private bool input_Shield;
    [NonSerialized] private bool input_Parry;
    [NonSerialized] private bool input_Attack;
    [NonSerialized] private float input_HorizontalMovement;
    [NonSerialized] private float input_VerticalMovement;

    // Player SFX
    [SerializeField] private AudioClip jumpSFX;
    [SerializeField] private AudioClip dashSFX;
    [SerializeField] private AudioClip attackSFX;
    [SerializeField] private AudioClip attackHitSFX;
    [SerializeField] private AudioClip hitSFX;

    // Animator
    Animator _animator;
    
    // Entity Values ========================




    // Cached Components ====================
    private Transform _playerTransform;
    private Rigidbody2D _playerRigidbody2D;
    private AudioSource _audioSource;

    //private Camera mainCamera;
    //private GameController _gameController;
    // Cached Components ====================



    // Cached GameObjects ====================
    [SerializeField] private GameObject playerProjectileAttack;// = GameObject.Find("Player_Attack");
    // Cached GameObjects ====================




    // Entity Functions =====================
    void RunInputs()
    {
        // Jump

        if (input_Jump)
        {
            _playerInput.Controller.Jump.canceled += ctx => input_Jump = false;
        }
        _playerInput.Controller.Jump.started += ctx => input_Jump = true;


        // Dash

        if (input_Dash)
        {
            _playerInput.Controller.Dash.canceled += ctx => input_Dash = false;
        }
        _playerInput.Controller.Dash.started += ctx => input_Dash = true;

        // Shield

        if (input_Shield)
        {
            _playerInput.Controller.Shield.canceled += ctx => input_Shield = false;
        }
        _playerInput.Controller.Shield.started += ctx => input_Shield = true;

        // Parry

        _playerInput.Controller.Parry.started += ctx =>
        {
            input_Parry = true;
        };
        _playerInput.Controller.Parry.canceled += ctx =>
        {
            input_Parry = false;
        };

        // Attack

        _playerInput.Controller.Attack.started += ctx =>
        {
            input_Attack = true;
        };
        _playerInput.Controller.Attack.canceled += ctx =>
        {
            input_Attack = false;
        };

        // Horizontal Movement

        input_HorizontalMovement = _playerInput.Keyboard.HorizontalMovement.ReadValue<float>();
        if (input_HorizontalMovement == 0)
        {
            input_HorizontalMovement = _playerInput.Controller.HorizontalMovement.ReadValue<float>();
        }

        // Vertical Movement

        input_VerticalMovement = _playerInput.Keyboard.VerticalMovement.ReadValue<float>();
        if (input_VerticalMovement == 0)
        {
            input_VerticalMovement = _playerInput.Controller.VerticalMovement.ReadValue<float>();
        }
    }

    void SetSprite()
    {

    }

    void DoAnimation()                                              // Arguments will be an int or enum that trigger that animation before returning to the idle animation.
    {

    }

    void CheckAttack()
    {
        if (startAttacking)
        {
            _animator.SetBool("isAttacking", false);
            startAttacking = false;
            canAttack = false;                                      // The player is unable to attack.
            attackCooldown = attackCooldown_length;                 // Begins the cooldown.
            attackCooldown_flag = true;                             // A flag is set to signify the cooldown is active.
            _audioSource.PlayOneShot(attackSFX);
        }
    }

    void CheckShield()
    {
        if (shieldBroken && !repairingShield)
        {
            repairingShield = true;
            canShield = false;                                       // The player is unable to shield.
            isShielding = false;
            shieldCooldown = shieldCooldown_length;                  // Begins the cooldown.
            shieldCooldown_flag = true;                              // A flag is set to signify the cooldown is active.
        }
    }


    void CheckParry()
    {
        if (startParrying)
        {
            startParrying = false;
            canParry = false;                                        // The player is unable to shield.
            isParrying = true;
            
            parryTimer = parryTimer_length;                          // Begins the cooldown.
            parryTimer_flag = true;                                  // A flag is set to signify the cooldown is active.
            
            parryCooldown = parryCooldown_length;                    // Begins the cooldown.
            parryCooldown_flag = true;                               // A flag is set to signify the cooldown is active.
        }
    }

    void CheckStunned()
    {
        if (startStunned)
        {
            startStunned = false;
            canMove = false;                                         // The player is unable to move.
            stunnedCooldown = stunnedCooldown_length;                // Begins the cooldown.
            stunnedCooldown_flag = true;                             // A flag is set to signify the cooldown is active.
        }
    }

    void CheckDash()
    {
        if (startDashing)
        {
            Dash();
            startDashing = false;
            dashTimer = dashTimer_length;                            // Begins the cooldown.
            dashTimer_flag = true;                                   // A flag is set to signify the cooldown is active.
        }
    }

    void Cooldown()
    {
        // Attack Cooldown
        if (attackCooldown > 0)
        {
            attackCooldown -= Time.deltaTime;

            if (attackCooldown < 0)
            {
                attackCooldown = 0;
            }

            if (canAttack)                                          // If is changed to true during testing, then the code accounts for it and bypasses the cooldown, ending it early.
            {
                attackCooldown = 0;
                attackCooldown_flag = false;
            }
        }
        else
        {
            if (attackCooldown_flag)
            {
                attackCooldown_flag = false;
                canAttack = true;
            }
        }

        // Shield Cooldown
        if (shieldCooldown > 0)
        {
            shieldCooldown -= Time.deltaTime;

            if (shieldCooldown < 0)
            {
                shieldCooldown = 0;
            }

            if (canShield)                                            // If is changed to true during testing, then the code accounts for it and bypasses the cooldown, ending it early.
            {
                shieldCooldown = 0;
                refreshShield = true;
                shieldBroken = false;
                repairingShield = false;
                shieldCooldown_flag = false;
            }
        }
        else
        {
            if (shieldCooldown_flag)
            {
                refreshShield = true;
                shieldBroken = false;
                repairingShield = false;
                shieldCooldown_flag = false;
                canShield = true;
            }
        }

        // Parry Cooldown
        if (parryTimer > 0)
        {
            parryTimer -= Time.deltaTime;

            if (parryTimer < 0)
            {
                parryTimer = 0;
            }

            if (canParry)                                            // If is changed to true during testing, then the code accounts for it and bypasses the cooldown, ending it early.
            {
                isParrying = false;
                parryTimer = 0;
                parryTimer_flag = false;
            }
        }
        else
        {
            if (parryTimer_flag)
            {
                isParrying = false;
                parryTimer_flag = false;
            }
        }
        if (parryCooldown > 0)
        {
            parryCooldown -= Time.deltaTime;

            if (parryCooldown < 0)
            {
                parryCooldown = 0;
            }

            if (canParry)                                            // If is changed to true during testing, then the code accounts for it and bypasses the cooldown, ending it early.
            {
                parryCooldown = 0;
                parryCooldown_flag = false;
            }
        }
        else
        {
            if (parryCooldown_flag)
            {
                parryCooldown_flag = false;
                canParry = true;
            }
        }

        // Stunned Cooldown
        if (stunnedCooldown > 0)
        {
            stunnedCooldown -= Time.deltaTime;

            if (stunnedCooldown < 0)
            {
                stunnedCooldown = 0;
            }

            if (canMove)                                            // If is changed to true during testing, then the code accounts for it and bypasses the cooldown, ending it early.
            {
                stunnedCooldown = 0;
                stunnedCooldown_flag = false;
            }
        }
        else
        {
            if (stunnedCooldown_flag)
            {
                stunnedCooldown_flag = false;
                canMove = true;
            }
        }

        // Dash Cooldown
        if (dashTimer > 0)
        {
            dashTimer -= Time.deltaTime;

            if (dashTimer < 0)
            {
                dashTimer = 0;
            }

            if (!canMove)                                            // If is changed to true during testing, then the code accounts for it and bypasses the cooldown, ending it early.
            {
                dashTimer = 0;
                dashTimer_flag = false;
                isDashing = false;
                _playerRigidbody2D.gravityScale = objGravity;
                _playerRigidbody2D.velocity = new Vector2(0, 0);
            }
        }
        else
        {
            if (dashTimer_flag)
            {
                dashTimer_flag = false;
                isDashing = false;
                _playerRigidbody2D.gravityScale = objGravity;
                _playerRigidbody2D.velocity = new Vector2(0, 0);
            }
        }
    }

    void Move()
    {
        showSmokeParticles = false;
        this.GetComponent<SpriteRenderer>().flipX = isFacingRight;

        if (canMove && !isShielding)
        {
            // Walking
            if (canJump)
            {
                float x = Input.GetAxisRaw("Horizontal Movement");
                if (x == 0)
                {
                    x = input_HorizontalMovement;
                    _animator.SetBool("isMoving", false);
                }
                else
                {
                    _animator.SetBool("isMoving", true);
                }
                float movementAmount = walkingSpeed;

                float moveBy = x * movementAmount;

                _playerRigidbody2D.velocity = new Vector2(moveBy, _playerRigidbody2D.velocity.y);

                if (Input.GetAxisRaw("Horizontal Movement") != 0 || input_HorizontalMovement != 0)
                {
                    showSmokeParticles = true;
                }

                if (Input.GetAxisRaw("Horizontal Movement") > 0 || input_HorizontalMovement > 0)
                {
                    isFacingRight = true;
                }
                if (Input.GetAxisRaw("Horizontal Movement") < 0 || input_HorizontalMovement < 0)
                {
                    isFacingRight = false;
                }
            }

            // Moving While Jumping
            if (!canJump && !isDashing)
            {
                float x = Input.GetAxisRaw("Horizontal Movement");
                if (x == 0)
                {
                    x = input_HorizontalMovement;
                }
                double movementAmount = (double)walkingSpeed;

                if (!((x < 0) && isCollidingHorizontally_left) && !((x > 0) && isCollidingHorizontally_right))
                {
                    movementAmount *= airborneDrift;
                    float moveSpeed = x * (float)movementAmount;

                    float moveBy = _playerRigidbody2D.velocity.x + moveSpeed;

                    if (moveBy > walkingSpeed)
                    {
                        moveBy = (float)walkingSpeed;
                    }
                    if (moveBy < -walkingSpeed)
                    {
                        moveBy = -(float)walkingSpeed;
                    }

                    //if ((moveBy < 0) && isCollidingHorizontally_left)
                    //{
                    //    moveBy = 2;
                    //}

                    //if ((moveBy > 0) && isCollidingHorizontally_right)
                    //{
                    //    moveBy = -2;
                    //}

                    _playerRigidbody2D.velocity = new Vector2(moveBy, _playerRigidbody2D.velocity.y);
                }
            }

            // Jumping & Dashing
            if (Input.GetAxisRaw("Jump") > 0 || Input.GetButton("Jump") || input_Jump || Input.GetAxisRaw("Dash") > 0 || Input.GetButton("Dash") || input_Dash)
            {
                if (!jumpKeyPressed)
                {
                    if (Input.GetAxisRaw("Dash") > 0 || Input.GetButton("Dash") || input_Dash)
                    {
                        // Dashing
                        if ((!canJump) && (canDash))
                        {
                            if (Input.GetAxisRaw("Horizontal Movement") != 0 || Input.GetAxisRaw("Vertical Movement") != 0)
                            {
                                canDash = false;
                                isDashing = true;
                                startDashing = true;

                                if (Input.GetAxisRaw("Horizontal Movement") != 0 || input_HorizontalMovement != 0)
                                {
                                    showSmokeParticles = true;
                                }

                                if (Input.GetAxisRaw("Horizontal Movement") > 0 || input_HorizontalMovement > 0)
                                {
                                    isFacingRight = true;
                                }
                                if (Input.GetAxisRaw("Horizontal Movement") < 0 || input_HorizontalMovement < 0)
                                {
                                    isFacingRight = false;
                                }

                                //PlaySound(int soundNo);
                            }
                        }
                    }
                    if (Input.GetAxisRaw("Jump") > 0 || Input.GetButton("Jump") || input_Jump)
                    {
                        // Jumping
                        if (canJump)
                        {
                            _playerRigidbody2D.velocity = new Vector2(_playerRigidbody2D.velocity.x, jumpStrength);
                            canJump = false;
                            canDash = true;

                            _audioSource.PlayOneShot(jumpSFX);
                        }
                    }


                }

                jumpKeyPressed = true;
            }
            else
            {
                jumpKeyPressed = false;
            }
        }

        if (isDashing)
        {
            showSmokeParticles = true;
        }

        if ((!canJump) && (canDash))
        {
            dashXTranslation = Input.GetAxisRaw("Horizontal Movement") * dashSpeed;
            if (Input.GetAxisRaw("Horizontal Movement") == 0)
            {
                dashXTranslation = input_HorizontalMovement * dashSpeed;
            }

            dashYTranslation = Input.GetAxisRaw("Vertical Movement") * dashSpeed * 0.85f;
            if (Input.GetAxisRaw("Vertical Movement") == 0)
            {
                dashYTranslation = input_VerticalMovement * dashSpeed * 0.85f;
            }
        }

    }

    void Attack()
    {
        if (Input.GetButton("Attack") || input_Attack)
        {
            if (!isShielding && !isParrying && !attackKeyPressed)
            {
                if (canAttack == true)
                {
                    startAttacking = true;
                    SpawnAttack();
                    _animator.SetBool("isAttacking", true);
                }
            }

            attackKeyPressed = true;
        }
        else
        {
            attackKeyPressed = false;
        }

    }

    void SpawnAttack()
    {
        float distance = 1.2f;
        //int rotation = 0;
        if (!isFacingRight)
        {
            distance *= -1;
        }

        //Transform attack_pos = playerProjectileAttack.transform;
        //attack_pos.position = new Vector3(_playerTransform.position.x + distance, _playerTransform.position.y, _playerTransform.position.z);

        GameObject attackProjectile = Instantiate(playerProjectileAttack, new Vector3(_playerTransform.position.x + distance, _playerTransform.position.y, _playerTransform.position.z), Quaternion.Euler(0, 0, 0));
        attackProjectile.GetComponent<Projectile_Default>().ownerCheck(this.gameObject, isFacingRight);
    }

    void Shield()
    {
        if (Input.GetAxisRaw("Shield") > 0 || Input.GetButton("Shield") || input_Shield)
        {
            if (canShield /*&& canJump*/)
            {
                if (!isShielding)
                {
                    if (canJump)
                    {
                        _playerRigidbody2D.velocity = new Vector2(0, _playerRigidbody2D.velocity.y);
                    }
                    else
                    {
                        _playerRigidbody2D.velocity = new Vector2(_playerRigidbody2D.velocity.x / 2, _playerRigidbody2D.velocity.y / 2);
                    }
                }

                isShielding = true;
                _animator.SetBool("isShielding", true);

                if (Input.GetAxisRaw("Parry") > 0 || Input.GetButton("Parry") || input_Parry)
                {
                    if (canParry == true)
                    {
                        startParrying = true;
                        //PlaySound(int soundNo);
                    }
                }
                else
                {
                }

            }
        }
        else
        {
            isShielding = false;
            _animator.SetBool("isShielding", false);
        }
    }

    void Dash()
    {
        _audioSource.PlayOneShot(dashSFX);

        _playerRigidbody2D.velocity = new Vector2(0, 0);
        _playerRigidbody2D.gravityScale = 0;

        _playerRigidbody2D.AddForce(new Vector3(dashXTranslation, dashYTranslation, 0));
        //_playerRigidbody2D.AddForce(Vector3.up * dashYTranslation);
    }

    void PlaySound(int soundNo)
    {
        switch (soundNo)
        {
            case 0:
                // Play Sound Here
                break;

            case 1:
                // Play Sound Here
                break;

            case 2:
                // Play Sound Here
                break;

            default:
                break;
        }
    }
    // Entity Functions =====================




    // Awake ================================
    void Awake()
    {
        _playerInput = new GameInput();
    }
    // Awake ================================

    private void OnEnable()
    {
        //_playerInput.Controller.Enable();
    }


    // Start ================================
    void Start()
    {
        // Cached Component Init
        _playerTransform = transform;
        _playerRigidbody2D = GetComponent<Rigidbody2D>();
        _audioSource = GetComponent<AudioSource>();
        _audioSource.volume = 0.45f;
        // mainCamera = Camera.main;
        // _AudioSource = GetComponent<AudioSource>();
        _animator = GetComponent<Animator>();

        health = health_max;
        dashTimer_length = dashLength;
        parryTimer_length = parryLength;
        objGravity = _playerRigidbody2D.gravityScale;
    }
    // Start ================================




    // Update ===============================
    void Update()
    {


            // Player Inputs and Controls
            RunInputs();

            // Checks and Cooldowns
            CheckAttack();
            CheckShield();
            CheckParry();
            CheckStunned();
            Cooldown();

        if (!isHurt)
        {
            _animator.SetBool("isHurt", false);

            isHurt_Knockback = false;
            // Player Movement & Abilities
            Move();
            CheckDash();
            Attack();
            Shield();

            if (health <= 0)
            {
                GameObject.Find("GameManager").gameObject.GetComponent<GameController>().ChangeState(GameController.EGameState.GameOver);
            }

            _animator.SetBool("isAirborne", !canJump);

        }
        else
        {
            _animator.SetBool("isHurt", true);

            // Cancellers
            isShielding = false;
            isParrying = false;
            dashTimer = 0;
            dashTimer_flag = false;
            isDashing = false;
            _playerRigidbody2D.gravityScale = objGravity;

            // Knockback
            if (!isHurt_Knockback)
            {
                isHurt_Knockback = true;
                _audioSource.PlayOneShot(hitSFX, 0.95f);
                if (isHurt_FromRight)
                {
                    _playerRigidbody2D.velocity = new Vector2(-hurtKnockback * 1.75f, hurtKnockback);
                }
                else
                {
                    _playerRigidbody2D.velocity = new Vector2(hurtKnockback * 1.75f, hurtKnockback);
                }
            }

            if (_playerRigidbody2D.velocity.x < 2 && _playerRigidbody2D.velocity.x > -2)
            {
                isHurt = false;
            }
        }

        _playerTransform.rotation = Quaternion.Euler(0, 0, 0);
    }
    // Update ===============================




    // 2D Collision Trigger =================
    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.CompareTag("Geometry") || other.CompareTag("One-Way Geometry"))
        {
            isDashing = false;

            // Checking if the collided object is below the player and if so, enable the player to jump again.
            if (other.gameObject.GetComponent<Rigidbody2D>().transform.position.y < this.gameObject.GetComponent<Rigidbody2D>().transform.position.y)
            {
                canJump = true;
                canDash = false;
            }

            if (other.gameObject.GetComponent<Rigidbody2D>().transform.position.x < this.gameObject.GetComponent<Rigidbody2D>().transform.position.x)
            {
                //isCollidingHorizontally_left = true;
            }

            if (other.gameObject.GetComponent<Rigidbody2D>().transform.position.x > this.gameObject.GetComponent<Rigidbody2D>().transform.position.x)
            {
                //isCollidingHorizontally_right = true;
            }
        }
    }
    
    private void OnTriggerExit2D(Collider2D other)
    {

        if (other.CompareTag("Geometry") || other.CompareTag("One-Way Geometry"))
        {
            // Checking if the collided object is below the player and if so, enable the player to jump again.
            if (other.gameObject.GetComponent<Rigidbody2D>().transform.position.y < this.gameObject.GetComponent<Rigidbody2D>().transform.position.y)
            {
                canJump = false;
                canDash = true;
            }

            if (other.gameObject.GetComponent<Rigidbody2D>().transform.position.x < this.gameObject.GetComponent<Rigidbody2D>().transform.position.x)
            {
                isCollidingHorizontally_left = false;
            }

            if (other.gameObject.GetComponent<Rigidbody2D>().transform.position.x > this.gameObject.GetComponent<Rigidbody2D>().transform.position.x)
            {
                isCollidingHorizontally_right = false;
            }
        }
    }
    // 2D Collision Trigger =================
}
