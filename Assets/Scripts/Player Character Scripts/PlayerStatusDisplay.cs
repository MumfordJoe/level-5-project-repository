using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using TMPro;

public class PlayerStatusDisplay : MonoBehaviour
{
    public enum DisplayInformation
    {
        Health,
        ParryAble,
        ShieldHealth,
        ShieldAble,
        Level
    }
    [SerializeField] DisplayInformation displayInformation = DisplayInformation.Health;
    private PlayerController _playerController;
    private PlayerShieldController _shieldController;
    private TMP_Text _textMesh;
    private string damageCheck;
    private Vector4 color;


    // Start is called before the first frame update
    void Start()
    {
        _playerController = GameObject.Find("Player").GetComponent<PlayerController>();
        _shieldController = GameObject.Find("Player_Shield").GetComponent<PlayerShieldController>();
        _textMesh = gameObject.GetComponent<TMP_Text>();
        color = _textMesh.color;
    }

    // Update is called once per frame
    void Update()
    {


        if (displayInformation == DisplayInformation.Health)
        {
            damageCheck = _textMesh.text;
            switch (_playerController.health)
            {
                default:
                    {
                        _textMesh.text = "G";
                        break;
                    }
                case 1:
                    {
                        _textMesh.text = "A";
                        break;
                    }
                case 2:
                    {
                        _textMesh.text = "B";
                        break;
                    }
                case 3:
                    {
                        _textMesh.text = "C";
                        break;
                    }
                case 4:
                    {
                        _textMesh.text = "D";
                        break;
                    }
                case 5:
                    {
                        _textMesh.text = "E";
                        break;
                    }
                case 6:
                    {
                        _textMesh.text = "EA";
                        break;

                    }
                case 7:
                    {
                        _textMesh.text = "EB";
                        break;
                    }
                case 8:
                    {
                        _textMesh.text = "EC";
                        break;
                    }
                case 9:
                    {
                        _textMesh.text = "ED";
                        break;
                    }
                case 10:
                    {
                        _textMesh.text = "EE";
                        break;
                    }
                case 11:
                    {
                        _textMesh.text = "EEA";
                        break;
                    }
                case 12:
                    {
                        _textMesh.text = "EEB";
                        break;
                    }
                case 13:
                    {
                        _textMesh.text = "EEC";
                        break;
                    }
                case 14:
                    {
                        _textMesh.text = "EED";
                        break;
                    }
                case 15:
                    {
                        _textMesh.text = "EEE";
                        break;
                    }
                case 16:
                    {
                        _textMesh.text = "EEEA";
                        break;
                    }
                case 17:
                    {
                        _textMesh.text = "EEEB";
                        break;
                    }
                case 18:
                    {
                        _textMesh.text = "EEEC";
                        break;
                    }
                case 19:
                    {
                        _textMesh.text = "EEED";
                        break;
                    }
                case 20:
                    {
                        _textMesh.text = "EEEE";
                        break;
                    }
                case 21:
                    {
                        _textMesh.text = "EEEEA";
                        break;
                    }
                case 22:
                    {
                        _textMesh.text = "EEEEB";
                        break;
                    }
                case 23:
                    {
                        _textMesh.text = "EEEEC";
                        break;
                    }
                case 24:
                    {
                        _textMesh.text = "EEEED";
                        break;
                    }
                case 25:
                    {
                        _textMesh.text = "EEEEE";
                        break;
                    }

            }
        }

        if (displayInformation == DisplayInformation.ShieldHealth)
        {
            switch (_shieldController.shieldHealth)
            {
                default:
                    {
                        _textMesh.text = " ";
                        break;
                    }
                case 1:
                    {
                        _textMesh.text = "A";
                        break;
                    }
                case 2:
                    {
                        _textMesh.text = "AA";
                        break;
                    }
                case 3:
                    {
                        _textMesh.text = "AAA";
                        break;
                    }
                case 4:
                    {
                        _textMesh.text = "AAAA";
                        break;
                    }
                case 5:
                    {
                        _textMesh.text = "AAAAA";
                        break;
                    }
                case 6:
                    {
                        _textMesh.text = "AAAAAA";
                        break;

                    }
            }
        }

        if (displayInformation == DisplayInformation.ParryAble)
        {
            if (_playerController.canParry)
            {
                _textMesh.text = "J";

                if (_playerController.canShield)
                {
                    _textMesh.color = new Vector4(color.x, color.y, color.z, 255);
                }
                else
                {
                    _textMesh.color = new Vector4(color.x, color.y, color.z, 50);
                }
            }
            else
            {
                _textMesh.text = " ";
                //_textMesh.color = new Vector4(0, 198, 255, 0);
            }
        }

        if (displayInformation == DisplayInformation.ShieldAble)
        {
            if (_playerController.canShield)
            {
                _textMesh.text = "Shield:";
                _textMesh.color = new Vector4(color.x, color.y, color.z, 255);
            }
            else
            {
                _textMesh.text = "<s> Shield: <s>";
                _textMesh.color = new Vector4(color.x, color.y, color.z, 50);
            }
        }

        if (displayInformation == DisplayInformation.Level)
        {
            _textMesh.color = new Vector4(255, 236, 160, 100);

            if (SceneManager.GetSceneByName("Level 1").isLoaded)
            {
                _textMesh.text = "Level 1";
            }
            if (SceneManager.GetSceneByName("Level 2").isLoaded)
            {
                _textMesh.text = "Level 2";
            }
            if (SceneManager.GetSceneByName("Testing Scene").isLoaded)
            {
                _textMesh.text = "Testing Level";
            }
        }
    }
}
