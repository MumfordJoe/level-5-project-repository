using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerParticlesController : MonoBehaviour
{
    //Cache
    ParticleSystem _ParticlesEnabled;

    //Vars
    //private int overrideParticles = 0;
    private GameObject playerObj;
    //private bool doParticles = false;

    // Start is called before the first frame update
    void Start()
    {
        _ParticlesEnabled = this.GetComponent<ParticleSystem>();
        playerObj = GameObject.Find("Player");
    }

    // Update is called once per frame
    void Update()
    {
        if (!playerObj.GetComponent<PlayerController>().showSmokeParticles)
        {
            //if (_ParticlesEnabled.isPlaying == true)
            //{
                _ParticlesEnabled.Stop(true, ParticleSystemStopBehavior.StopEmitting);
            //}
        }
        else
        {
            //if (_ParticlesEnabled.isPlaying == false)
            //{
                _ParticlesEnabled.Play(true);
            //}
        }
    }
}
