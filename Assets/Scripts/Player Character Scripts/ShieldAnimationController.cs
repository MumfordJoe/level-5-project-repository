using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShieldAnimationController : MonoBehaviour
{
    // Animator
    Animator _animator;
    PlayerController _playerController;
    Transform _transform;
    GameObject _player;
    SpriteRenderer _spriteRenderer;
    float distance_original;

    // Start is called before the first frame update
    void Start()
    {
        _animator = GetComponent<Animator>();
        _player = GameObject.Find("Player");
        _playerController = GameObject.Find("Player").GetComponent<PlayerController>();
        _transform = transform;
        _spriteRenderer = GetComponent<SpriteRenderer>();
    }

    // Update is called once per frame
    void Update()
    {
        float distance = distance_original;
        float yoffset = 0;

        if (_playerController.isShielding)
        {
            _animator.SetBool("isShielding", true);
            distance = distance_original + 0.75f;
            yoffset = 0.05f;
            if (_playerController.isParrying)
            {
                _spriteRenderer.color = _spriteRenderer.color = new Vector4(0, 255, 255, 255);
            }
            else
            {
                _spriteRenderer.color = _spriteRenderer.color = new Vector4(255, 255, 255, 255);
            }
        }
        else
        {
            _animator.SetBool("isShielding", false);
            distance = distance_original - 0.625f;
            yoffset = -0.15f;
            _spriteRenderer.color = _spriteRenderer.color = new Vector4(255, 255, 255, 255);
        }

        this.GetComponent<SpriteRenderer>().flipX = _playerController.isFacingRight;

        float distance_current = distance;

        if (!_playerController.isFacingRight)
        {
            distance_current = distance * -1;
        }

        _transform.position = new Vector3(_player.transform.position.x + distance_current, _player.transform.position.y - 0.0f + yoffset, _player.transform.position.z - 0.25f);
    }
}
