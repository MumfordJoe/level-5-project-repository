using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerAttack : MonoBehaviour
{
    [NonSerialized] private GameObject playerObj;
    [NonSerialized] private Transform _transform;
    private float distance;

    // Start is called before the first frame update
    void Start()
    {
        playerObj = GameObject.Find("Player");
        _transform = transform;

        distance = playerObj.transform.position.x - _transform.position.x;
    }

    // Update is called once per frame
    void Update()
    {
        if (playerObj != null)
        {
            _transform.position = new Vector3(playerObj.transform.position.x - distance, playerObj.transform.position.y, playerObj.transform.position.z);
        }
        else
        {
            Destroy(gameObject);
        }
    }
}
