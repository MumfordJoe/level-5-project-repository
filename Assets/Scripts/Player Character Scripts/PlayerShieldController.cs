using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// This entity's prefab is to be attached to the player object's prefab when in use.

public class PlayerShieldController : MonoBehaviour
{
    [NonSerialized] private SpriteRenderer _spriteRenderer;
    [NonSerialized] private GameObject playerObj;
    [NonSerialized] private Transform _transform;
    [NonSerialized] private Color setColor;
    [NonSerialized] private float distanceFromPlayer = 0.75f;
    [NonSerialized] private bool ignoreProjectiles = true;
    [NonSerialized] public bool isParrying = false;
    [NonSerialized] private float gravityProjectileReflectionYForce = 400.0f;

    [SerializeField] public int shieldHealth;

    [NonSerialized] private AudioSource _audioSource;
    [SerializeField] private AudioClip blockSFX;
    [SerializeField] private AudioClip parrySFX;

    // Start is called before the first frame update
    void Start()
    {
        _spriteRenderer = this.GetComponent<SpriteRenderer>();
        playerObj = GameObject.Find("Player");
        _transform = transform;
        setColor = _spriteRenderer.color;
        isParrying = playerObj.GetComponent<PlayerController>().isParrying;
        shieldHealth = playerObj.GetComponent<PlayerController>().shieldHealth_max;
        _audioSource = GetComponent<AudioSource>();
        _audioSource.volume = 0.20f;
    }

    // Update is called once per frame
    void Update()
    {
        if (playerObj.GetComponent<PlayerController>().refreshShield)
        {
            playerObj.GetComponent<PlayerController>().refreshShield = false;
            shieldHealth = playerObj.GetComponent<PlayerController>().shieldHealth_max;
        }

        if (shieldHealth <= 0)
        {

            playerObj.GetComponent<PlayerController>().shieldBroken = true;

        }

        if (playerObj.GetComponent<PlayerController>().canShield)
        {
            isParrying = playerObj.GetComponent<PlayerController>().isParrying;

            if (playerObj.GetComponent<PlayerController>().isShielding)
            {
                if (playerObj.GetComponent<PlayerController>().isParrying)
                {
                    _spriteRenderer.color = new Vector4(0, 255, 255, 0);//100); // Light Blue
                    ignoreProjectiles = false;
                }
                else
                {
                    _spriteRenderer.color = setColor; // Starting Color (Blue)
                    ignoreProjectiles = false;
                }
            }
            else
            {
                _spriteRenderer.color = new Vector4(100, 100, 100, 0);//100); // Grey
                ignoreProjectiles = true;
            }


            if (playerObj.GetComponent<PlayerController>().isFacingRight)
            {
                _transform.position = new Vector3(playerObj.transform.position.x + distanceFromPlayer, playerObj.transform.position.y, playerObj.transform.position.z);
            }
            else
            {
                _transform.position = new Vector3(playerObj.transform.position.x - distanceFromPlayer, playerObj.transform.position.y, playerObj.transform.position.z);
            }
        }
        else
        {
            _spriteRenderer.color = new Vector4(255, 0, 0, 0);//100); // Red
        }
    }

    // 2D Collision Trigger =================
    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.CompareTag("Projectile_Enemy"))
        {
            Physics2D.IgnoreCollision(other.GetComponent<PolygonCollider2D>(), GetComponent<PolygonCollider2D>(), ignoreProjectiles);

            if (playerObj.GetComponent<PlayerController>().isShielding)
            {
                if (isParrying)
                {
                    _audioSource.PlayOneShot(parrySFX);

                    playerObj.GetComponent<PlayerController>().isShielding = false;
                    playerObj.GetComponent<PlayerController>().isParrying = false;

                    // Changing pos and tag of projectile.
                    other.gameObject.tag = "Projectile_Player";
                    other.gameObject.layer = LayerMask.NameToLayer("Projectiles_Player");
                    if (other.GetComponent<Projectile_Default>().isHoming)
                    {
                        other.GetComponent<Projectile_Default>().lockedOn = false;
                        other.GetComponent<Projectile_Default>().homingTarget = null;
                    }
                    other.GetComponent<Projectile_Default>().collisionTargetTag = "Enemy";

                    if (other.gameObject.GetComponent<Rigidbody2D>().gravityScale == 0)
                    {
                        if (playerObj.GetComponent<PlayerController>().isFacingRight)
                        {
                            other.transform.rotation = Quaternion.Euler(other.transform.rotation.x, other.transform.rotation.y, -90);
                        }
                        else
                        {
                            other.transform.rotation = Quaternion.Euler(other.transform.rotation.x, other.transform.rotation.y, 90);
                        }
                    }
                    else
                    {
                        other.gameObject.GetComponent<Rigidbody2D>().AddForce(new Vector3(0, gravityProjectileReflectionYForce, 0));
                        other.GetComponent<Projectile_Default>().projectileSpeed *= 1.5f;

                        if (playerObj.GetComponent<PlayerController>().isFacingRight)
                        {
                            other.transform.rotation = Quaternion.Euler(other.transform.rotation.x, other.transform.rotation.y, -other.GetComponent<Projectile_Default>().projectileFiringAngle + (-other.GetComponent<Projectile_Default>().projectileFiringAngle / 2));

                        }
                        else
                        {
                            other.transform.rotation = Quaternion.Euler(other.transform.rotation.x, other.transform.rotation.y, -other.GetComponent<Projectile_Default>().projectileFiringAngle + (other.GetComponent<Projectile_Default>().projectileFiringAngle / 2));
                        }
                    }
                }
                else
                {
                    _audioSource.PlayOneShot(blockSFX);
                    shieldHealth -= 1; //other.GetComponent<Projectile_Default>().damage;
                    Destroy(other.gameObject);
                }
            }
            else
            {
                Physics2D.IgnoreCollision(other.GetComponent<PolygonCollider2D>(), GetComponent<PolygonCollider2D>(), ignoreProjectiles);
            }
        }
    }

    private void OnCollisionEnter2D(Collision2D other)
    {
        if (other.gameObject.CompareTag("Enemy"))
        {
            Physics2D.IgnoreCollision(other.gameObject.GetComponent<PolygonCollider2D>(), GetComponent<PolygonCollider2D>(), false);

            if (playerObj.GetComponent<PlayerController>().isShielding)
            {
                if (isParrying)
                {
                    if (other.gameObject.GetComponent<EnemyController_Parent>().currentState == EnemyController_Parent.EnemyStates.Rolling)
                    {
                        if (!other.gameObject.GetComponent<EnemyController_Parent>().hasRollCollision)
                        {
                            _audioSource.PlayOneShot(parrySFX);
                            other.gameObject.GetComponent<EnemyController_Parent>().hasRollCollision = true;
                            other.gameObject.GetComponent<EnemyController_Parent>()._enemyRigidbody2D.velocity = new Vector2(-other.gameObject.GetComponent<EnemyController_Parent>()._enemyRigidbody2D.velocity.x * 2.25f, other.gameObject.GetComponent<EnemyController_Parent>()._enemyRigidbody2D.velocity.y);
                            other.gameObject.GetComponent<EnemyController_Parent>().health -= 1;
                        }
                    }
                }
                else
                {
                    if (other.gameObject.GetComponent<EnemyController_Parent>().currentState == EnemyController_Parent.EnemyStates.Rolling)
                    {
                        if (!other.gameObject.GetComponent<EnemyController_Parent>().hasRollCollision)
                        {
                            _audioSource.PlayOneShot(blockSFX);
                            other.gameObject.GetComponent<EnemyController_Parent>().hasRollCollision = true;
                            other.gameObject.GetComponent<EnemyController_Parent>()._enemyRigidbody2D.velocity = new Vector2(-other.gameObject.GetComponent<EnemyController_Parent>()._enemyRigidbody2D.velocity.x * 1.75f, other.gameObject.GetComponent<EnemyController_Parent>()._enemyRigidbody2D.velocity.y);

                            shieldHealth -= 1; //other.gameObject.GetComponent<EnemyController_Parent>().attackStrength;
                        }
                    }
                }
            }
            else
            {
                //Physics2D.IgnoreCollision(other.gameObject.GetComponent<PolygonCollider2D>(), GetComponent<PolygonCollider2D>(), true);
            }

        }
    }
        // 2D Collision Trigger =================

}
