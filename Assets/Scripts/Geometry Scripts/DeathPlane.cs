using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DeathPlane : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.CompareTag("Player"))
        {
            other.gameObject.GetComponent<PlayerController>().health = 0;
        }
        if (other.gameObject.CompareTag("Enemy"))
        {
            Physics2D.IgnoreCollision(other.gameObject.GetComponent<PolygonCollider2D>(), GetComponent<PolygonCollider2D>(), true);
        }
    }
        //private void OnCollisionEnter2D(Collider2D other)
        //{
        //    if (other.CompareTag("Player"))
        //    {
        //        other.gameObject.GetComponent<PlayerController>().health = 0;
        //    }
        //}
}

