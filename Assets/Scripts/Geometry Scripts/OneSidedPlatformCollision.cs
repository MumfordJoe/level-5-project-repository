using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OneSidedPlatformCollision : MonoBehaviour
{
    // Vars
    Rigidbody2D _platformRigidbody2D;
    private GameObject playerObj;

    // Start is called before the first frame update
    void Start()
    {
        _platformRigidbody2D = this.GetComponent<Rigidbody2D>();
        playerObj = GameObject.Find("Player");
    }

    // Update is called once per frame
    void Update()
    {
        if ((playerObj.transform.position.y <= this.transform.position.y) || playerObj.GetComponent<PlayerController>().dashTimer > 0)
        {
            Physics2D.IgnoreCollision(playerObj.GetComponent<PolygonCollider2D>(), GetComponent<BoxCollider2D>(), true);
        }
        else
        {
            Physics2D.IgnoreCollision(playerObj.GetComponent<PolygonCollider2D>(), GetComponent<BoxCollider2D>(), false);
        }
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.tag == "Enemy")
        {
            if ((collision.gameObject.transform.position.y <= this.transform.position.y))
            {
                Physics2D.IgnoreCollision(collision.gameObject.GetComponent<PolygonCollider2D>(), GetComponent<BoxCollider2D>(), true);
            }
            else
            {
                Physics2D.IgnoreCollision(collision.gameObject.GetComponent<PolygonCollider2D>(), GetComponent<BoxCollider2D>(), false);
            }
        }
    }

}
