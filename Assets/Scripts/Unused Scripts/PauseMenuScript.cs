using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PauseMenuScript : MonoBehaviour
{
    public GameObject PauseCanvas;
    public GameObject ControlCanvas;
    bool isPaused;
    bool controlsActive;

    private void Start()
    {
        isPaused = false;
        controlsActive = false;
    }
    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            if (isPaused == false)
            {
                Pause();
            }
            else
            {
                UnPause();
            }
        }
    }

    private void Pause()
    {
        isPaused = true;
        Time.timeScale = 0f;
        PauseCanvas.SetActive(true);
    }

    private void UnPause()
    {
        isPaused = false;
        Time.timeScale = 1f;
        PauseCanvas.SetActive(false);
    }

    public void ResumeButton()
    {
        UnPause();
    }
    public void ControlButton()
    {
        if (controlsActive == false)
        {
            ControlCanvas.SetActive(true);
            controlsActive = true;
        }
        else
        {
            ControlCanvas.SetActive(false);
            controlsActive = false;
        }
    }

    public void ExitButton()
    {
        SceneManager.LoadScene("MainMenu");
    }
}
