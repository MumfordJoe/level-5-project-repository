using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MainMenuScript : MonoBehaviour
{
    public GameObject MainCanvas;
    public GameObject ControlCanvas;
    public GameObject CreditCanvas;
    private GameObject ChosenCanvas;

    private void Start()
    {
        ControlCanvas.SetActive(false);
        CreditCanvas.SetActive(false);
    }

    public void PlayButton()
    {
        SceneManager.LoadScene("Level 1");
    }

    public void ControlsButton()
    {
        ChosenCanvas = ControlCanvas;
        ChangeCanvas();
    }

    public void CreditsButton()
    {
        ChosenCanvas = CreditCanvas;
        ChangeCanvas();
    }

    public void MenuButton()
    {
        ChosenCanvas.SetActive(false);
        MainCanvas.SetActive(true);
    }

    public void QuitButton()
    {
        Application.Quit();
        Debug.Log("QUIT GAME");
    }

    private void ChangeCanvas()
    {
        MainCanvas.SetActive(false);
        ChosenCanvas.SetActive(true);
    }

}
