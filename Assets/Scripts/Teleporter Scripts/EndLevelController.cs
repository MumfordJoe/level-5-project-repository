using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class EndLevelController : MonoBehaviour
{
    private GameObject _player;
    private GameObject _teleporterExit;
    private Camera _camera;
    private int tpNum;

    // Start is called before the first frame update
    void Start()
    {
        _camera = Camera.main;
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.CompareTag("Player"))
        {
            if (SceneManager.GetSceneByName("Level 1").isLoaded)
            {
                GameObject.Find("GameManager").gameObject.GetComponent<GameController>().gameLevel = 1;
            }
            if (SceneManager.GetSceneByName("Level 2").isLoaded)
            {
                GameObject.Find("GameManager").gameObject.GetComponent<GameController>().gameLevel = 2;
            }

            GameObject.Find("GameManager").gameObject.GetComponent<GameController>().ChangeState(GameController.EGameState.Victory);

        }
    }
}
