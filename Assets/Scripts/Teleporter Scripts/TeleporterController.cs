using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TeleporterController : MonoBehaviour
{
    private GameObject _player;
    private GameObject _teleporterExit;
    private Camera _camera;
    private int tpNum;

    // Start is called before the first frame update
    void Start()
    {
        _player = GameObject.Find("Player");
        if (gameObject.name == "Teleporter 1 Entrance")
        {
            _teleporterExit = GameObject.Find("Teleporter 1 Exit");
            tpNum = 1;
        }
        if (gameObject.name == "Teleporter 2 Entrance")
        {
            _teleporterExit = GameObject.Find("Teleporter 2 Exit");
            tpNum = 2;
        }
        if (gameObject.name == "Teleporter 3 Entrance")
        {
            _teleporterExit = GameObject.Find("Teleporter 3 Exit");
            tpNum = 3;
        }

        _camera = Camera.main;
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.CompareTag("Player"))
        {
            other.transform.position = new Vector3(_teleporterExit.transform.position.x, _teleporterExit.transform.position.y, _teleporterExit.transform.position.z);
            other.GetComponent<PlayerController>().canJump = false;

            GameObject _camLimits = GameObject.Find("Camera Limits");
            _camLimits.GetComponent<CameraController>().limitsNumber = tpNum + 1;


            //other.GetComponent<PlayerController>().canDash = false;
            //_camera.GetComponent<CinemachineConfiner>.BoundingShape2D;
        }
    }
}
