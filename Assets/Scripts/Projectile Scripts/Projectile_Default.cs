using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Projectile_Default : MonoBehaviour
{
    // Projectile Values
    [Tooltip("The speed at which the projectile moves every frame.")]
    [SerializeField] public float projectileSpeed = 10.0f;
    [Tooltip("The direction the projectile moves, this being anticlockwise from directly upwards. Ideally betweeen -75 and 75.")]
    [SerializeField] public float projectileFiringAngle = 0.0f;
    [Tooltip("If you wish for the projectile to have gravity then set this ideally between  0.1 and 0.5. Otherwise leave it at 0.")]
    [SerializeField] private float projectileGravity = 0.0f;
    [Tooltip("The amount of damage the projectile will do to it's collision target.")]
    [SerializeField] public int damage = 1;
    [Tooltip("The tag of the entity the projectile is targeting. Is 'Player' by default, but can be chaged to 'Enemy' as well..")]
    [SerializeField] public string collisionTargetTag = "Player";
    [Tooltip("Whether the projectile will disappear after a set time or not.")]
    [SerializeField] private bool hasLifespan = false;
    [Tooltip("The length of the projectile's lifespan - in seconds. Doesn't do anything if lifespan is disabled.")]
    [SerializeField] private float projectileLifespan = 10.0f;
    [NonSerialized] private float projectileLifespan_current;

    // Homing Projectile Values
    [SerializeField] public bool isHoming = false;
    [Tooltip("The angle the projectile will rotate every frame to align itself with it's target.")]
    [SerializeField] private float homingRotationSpeed = 4.0f;
    [Tooltip("The radius the projectile will search for the targeted objects in the list.")]
    [SerializeField] private float targetSearchRange = 5.0f;
    [Tooltip("A list of all of the enemies/objects that are within the projectile's radius.")]
    [NonSerialized] private List<GameObject> nearbyObjects = new List<GameObject>();
    [Tooltip("Whether the projectile is locked onto a target or not.")]
    [NonSerialized] public bool lockedOn = false;
    [Tooltip("The projectile's current homing target.")]
    [NonSerialized] public GameObject homingTarget;

    // Cache
    //private GameController _gameController;
    private Rigidbody2D _projectileRigidbody2D;
    private GameObject ownerObject;
    private AudioSource _audio;
    [SerializeField] private AudioClip slashHit;
    [SerializeField] private AudioClip projectileSpawn;

    //Determines if Slash Hit Sound should play.
    [SerializeField] private bool isSlash;

    bool ArrayContains(GameObject[] array, GameObject g)
    {
        for (int i = 0; i < array.Length; i++)
        {
            if (array[i] == g) return true;
        }
        return false;
    }

    void BubbleSortDistance(List<GameObject> arr)
    {
        GameObject temp;

        for (int write = 0; write < arr.Count; write++)
        {
            for (int sort = 0; sort < arr.Count - 1; sort++)
            {
                float distance1 = Vector3.Distance(arr[sort].transform.position, _transform.position);
                float distance2 = Vector3.Distance(arr[sort + 1].transform.position, _transform.position);
                if (distance1 > distance2)
                {
                    temp = arr[sort + 1];
                    arr[sort + 1] = arr[sort];
                    arr[sort] = temp;
                }
            }
        }
    }

    private void Homing()
    {
        if (lockedOn == false)
        {
            // Finds all nearby enemies within the projectile's lock-on radius.
            GameObject[] possibleTargets = GameObject.FindGameObjectsWithTag(collisionTargetTag);
            nearbyObjects.Clear();
            for (int i = 0; i < possibleTargets.Length; i++)
            {
                float distance = Vector3.Distance(possibleTargets[i].transform.position, _transform.position);
                if (distance <= targetSearchRange)
                {
                    nearbyObjects.Add(possibleTargets[i]);
                }
            }

            // Sorts all of the nearby enemies to find which one is closest and sets it as the target.
            if (nearbyObjects.Count > 0)
            {
                BubbleSortDistance(nearbyObjects);
                homingTarget = nearbyObjects[0];
                lockedOn = true;
            }
        }
        else
        {
            // Resets the projectile if the target is non-existant. (Prevents crashes.)
            if (homingTarget == null)
            {
                lockedOn = false;
            }
            else
            {
                // Projectile homing code that makes the project home in on it's target enemy.
                Vector3 targetPos = homingTarget.transform.position;
                Vector3 currentPos = _transform.position;

                Vector3 delta = targetPos - currentPos;
                float angle = Mathf.Atan2(delta.y, delta.x) * Mathf.Rad2Deg - 90;
                Quaternion rotation = Quaternion.Euler(0, 0, angle);

                _transform.rotation = Quaternion.Slerp(_transform.rotation, rotation, Time.deltaTime * homingRotationSpeed);
            }
        }
    }

    private void OnBecameInvisible()
    {
        Destroy(gameObject);
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.CompareTag(collisionTargetTag))
        {

            if (other.CompareTag("Player"))
            {
                // The collided object taking damage.
                other.gameObject.GetComponent<PlayerController>().isHurt = true;
                if (this.transform.position.x <= other.gameObject.transform.position.x)
                {
                    other.gameObject.GetComponent<PlayerController>().isHurt_FromRight = false;
                }
                else
                {
                    other.gameObject.GetComponent<PlayerController>().isHurt_FromRight = true;
                }
                other.gameObject.GetComponent<PlayerController>().health -= damage;
            }
            if (other.CompareTag("Enemy"))
            {
                if (isSlash)
                {
                    _audio.PlayOneShot(slashHit);
                }

                // The collided object taking damage.
                other.gameObject.GetComponent<EnemyController_Parent>().isHurt = true;
                if (this.transform.position.x <= other.gameObject.transform.position.x)
                {
                    other.gameObject.GetComponent<EnemyController_Parent>().isHurt_FromRight = false;
                }
                else
                {
                    other.gameObject.GetComponent<EnemyController_Parent>().isHurt_FromRight = true;
                }
                other.gameObject.GetComponent<EnemyController_Parent>().health -= damage;
            }

            // Destroy Bullet / Lazer
            Destroy(gameObject);
        }
        if (other.CompareTag("Geometry"))
        {
            if (!(gameObject.name == "Player_Attack(Clone)") && !(gameObject.name == "Player_Attack"))
            {
                // Destroy Bullet / Lazer
                Destroy(gameObject);
            }
        }
    }

   public void ownerCheck(GameObject owner, bool isfacingRight)
    {
        if (ownerObject == null)
        {
            _transform = transform;

            if (owner.tag == "Player")
            {
                if (!owner.GetComponent<PlayerController>().isFacingRight || !isfacingRight)
                {
                    //_transform.rotation = Quaternion.Euler(_transform.rotation.x, _transform.rotation.y, _transform.rotation.z - 180);

                    gameObject.transform.GetChild(0).GetComponent<SpriteRenderer>().flipX = true; //GetComponent<Player_Attack_Sprite>().flipX = true;
                }
            }

            if (owner.tag == "Enemy")
            {
                if (owner.GetComponent<EnemyController_Parent>().facingRight || isfacingRight)
                {
                    //_transform.rotation = Quaternion.Euler(_transform.rotation.x, _transform.rotation.y, _transform.rotation.z - 180);
                    projectileFiringAngle *= -1;
                }
            }

            ownerObject = owner;
        }
    }

    private void Awake()
    {
        if (gameObject.name == "Enemy_Projectile_DarkWolf" || gameObject.name == "Enemy_Projectile_DarkWolf (Clone)")
        {
            gameObject.GetComponent<ParticleSystem>().Play(true);
        }
    }

    private Transform _transform;
    void Start()
    {
        //if (SceneManager.GetSceneByName("Management").isLoaded == true)
        //{
        //    _gameController = GameObject.Find("GameManager").GetComponent<GameController>();
        //    Debug.Assert(_gameController, this.gameObject);
        //}

        _transform = transform;
        _projectileRigidbody2D = GetComponent<Rigidbody2D>();

        _transform.rotation = Quaternion.Euler (_transform.rotation.x, _transform.rotation.y, projectileFiringAngle);
        _projectileRigidbody2D.gravityScale = projectileGravity;
        projectileLifespan_current = projectileLifespan;

        _audio = GetComponent<AudioSource>();


        if (Vector3.Distance(GameObject.Find("Player").transform.position, transform.position) < 10)
        {
            _audio.volume = 0.15f;
            _audio.PlayOneShot(projectileSpawn);
        }
    }

    void Update()
    {

        if (isHoming)
        {
            Homing();
        }

        if (hasLifespan)
        {
            projectileLifespan_current -= Time.deltaTime;
            if (projectileLifespan_current <= 0)
            {
                // Destroy Bullet / Lazer
                Destroy(gameObject);
            }
        }

        // Movement
        Vector3 translateAmount = Vector3.up * (Time.deltaTime * projectileSpeed);
        _transform.Translate(translateAmount);

        //// Deletes this object when it strays too far from the camera's view. (Backup)
        //if ((this.transform.position.x > Camera.main.transform.position.x + Camera.main.orthographicSize * 3) || (this.transform.position.x < Camera.main.transform.position.x - Camera.main.orthographicSize * 3))
        //    Destroy(this.gameObject);
        //if ((this.transform.position.y > Camera.main.transform.position.y + Camera.main.aspect * Camera.main.orthographicSize * 2) || (this.transform.position.y < Camera.main.transform.position.y - Camera.main.aspect * Camera.main.orthographicSize * 2))
        //    Destroy(this.gameObject);

    }
}
