﻿//using UnityEngine;
//using UnityEngine.SceneManagement;
//using System;
//using System.Collections;
//using System.Collections.Generic;

//public class HomingProjectile : MonoBehaviour
//{
//    [SerializeField] private float projectileSpeed = 8.0f;
//    [SerializeField] private float homingRotationSpeed = 4.0f;     // The angle the projectile will rotate every frame to align itself with it's target.
//    [SerializeField] private float targetSearchRange = 5.0f;            // The radius the projectile will search for the targeted objects in the list.
//    [SerializeField] private List<GameObject> nearbyObjects = new List<GameObject>();   // A list of all of the enemies/objects that are within the projectile's radius.
//    [SerializeField] private bool lockedOn = false;                     // Whether the projectile is locked onto a target or not.
//    [SerializeField] private GameObject target;                         // The projectile's target.
//    [SerializeField] private int damage = 2;

//    // Cache
//    private GameController _gameController;

//    private void OnBecameInvisible()
//    {
//        Destroy(gameObject);
//    }

//    private void OnTriggerEnter2D(Collider2D other)
//    {
//        if ((other.CompareTag("Enemy")) || (other.CompareTag("Enemy Boss")))
//        {
//            if (other.gameObject.GetComponent<Tier1EnemyBehaviour>() != null)
//            {
//                // The collided object taking damage.
//                other.gameObject.GetComponent<Tier1EnemyBehaviour>().health -= damage;
//            }

//            if (other.gameObject.GetComponent<Tier2EnemyBehaviour>() != null)
//            {
//                // The collided object taking damage.
//                other.gameObject.GetComponent<Tier2EnemyBehaviour>().health -= damage;
//            }

//            if (other.gameObject.GetComponent<Tier3EnemyBehaviour>() != null)
//            {
//                // The collided object taking damage.
//                other.gameObject.GetComponent<Tier3EnemyBehaviour>().health -= damage;
//            }

//            // Destroy Bullet / Lazer
//            Destroy(gameObject);
//        }

//        if (other.CompareTag("Enemy Rocket"))
//        {
//            // The collided object taking damage.
//            Destroy(other.gameObject); 

//            // Destroy Bullet / Lazer
//            Destroy(gameObject);
//        }
//    }

//    private Transform _transform;
//    void Start()
//    {
//        if (SceneManager.GetSceneByName("Management").isLoaded == true)
//        {
//            _gameController = GameObject.Find("GameManager").GetComponent<GameController>();
//            Debug.Assert(_gameController, this.gameObject);
//        }

//        _transform = transform;
//    }

//    bool ArrayContains(GameObject[] array, GameObject g)
//    {
//        for (int i = 0; i < array.Length; i++)
//        {
//            if (array[i] == g) return true;
//        }
//        return false;
//    }
    
//    void BubbleSortDistance(List<GameObject> arr)
//    {
//        GameObject temp;

//        for (int write = 0; write < arr.Count; write++)
//        {
//            for (int sort = 0; sort < arr.Count - 1; sort++)
//            {
//                float distance1 = Vector3.Distance(arr[sort].transform.position, _transform.position);
//                float distance2 = Vector3.Distance(arr[sort + 1].transform.position, _transform.position);
//                if (distance1 > distance2)
//                {
//                    temp = arr[sort + 1];
//                    arr[sort + 1] = arr[sort];
//                    arr[sort] = temp;
//                }
//            }
//        }
//    }

//    void Update()
//    {
//        if (_gameController.GetState() == GameController.EGameState.Playing)
//        {
//            // Get a translation up (forward of the sprite in 2D)
//            // Multiply by time so we aren't frame-rate dependant
//            // Multiply by Move Speed so we can tweak it
//            Vector3 translateAmount = Vector3.up * (Time.deltaTime * projectileSpeed);
//            // Apply the translation
//            _transform.Translate(translateAmount);

//            if (lockedOn == false)
//            {
//                // Finds all nearby enemies within the projectile's lock-on radius.
//                GameObject[] possibleTargets = GameObject.FindGameObjectsWithTag("Enemy");
//                nearbyObjects.Clear();
//                for (int i = 0; i < possibleTargets.Length; i++)
//                {
//                    float distance = Vector3.Distance(possibleTargets[i].transform.position, _transform.position);
//                    if (distance <= targetSearchRange)
//                    {
//                        nearbyObjects.Add(possibleTargets[i]);
//                    }
//                }

//                // Sorts all of the nearby enemies to find which one is closest and sets it as the target.
//                if (nearbyObjects.Count > 0)
//                {
//                    BubbleSortDistance(nearbyObjects);
//                    target = nearbyObjects[0];
//                    lockedOn = true;
//                }
//            }
//            else
//            {
//                // Resets the projectile if the target is non-existant. (Prevents crashes.)
//                if (target == null)
//                {
//                    lockedOn = false;
//                }
//                else
//                {
//                    // Projectile homing code that makes the project home in on it's target enemy.
//                    Vector3 targetPos = target.transform.position;
//                    Vector3 currentPos = _transform.position;

//                    Vector3 delta = targetPos - currentPos;
//                    float angle = Mathf.Atan2(delta.y, delta.x) * Mathf.Rad2Deg - 90;
//                    Quaternion rotation = Quaternion.Euler(0, 0, angle);

//                    _transform.rotation = Quaternion.Slerp(_transform.rotation, rotation, Time.deltaTime * homingRotationSpeed);
//                }
//            }

//            // Deletes this object when it strays too far from the camera's view.
//            if ((this.transform.position.x > Camera.main.transform.position.x + Camera.main.orthographicSize * 3) || (this.transform.position.x < Camera.main.transform.position.x - Camera.main.orthographicSize * 3))
//                Destroy(this.gameObject);
//            if ((this.transform.position.y > Camera.main.transform.position.y + Camera.main.aspect * Camera.main.orthographicSize * 2) || (this.transform.position.y < Camera.main.transform.position.y - Camera.main.aspect * Camera.main.orthographicSize * 2))
//                Destroy(this.gameObject);
//        }
//    }
//}
