using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class poofSFXEmitterScript : MonoBehaviour
{
    [NonSerialized] private AudioSource _audioSource;
    [SerializeField] private AudioClip poofSFX;
    private bool played = false;

    // Start is called before the first frame update
    void Start()
    {
        _audioSource = GetComponent<AudioSource>();
        _audioSource.volume = 1.35f;

        _audioSource.PlayOneShot(poofSFX);
        played = true;
    }

    // Update is called once per frame
    void Update()
    {
        if (_audioSource.isPlaying == false && played == true)
        {
            Destroy(gameObject);
        }
    }
}
