using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class EnemyDeathSmokeController : MonoBehaviour
{
    private float timeTillDeath = 0.55f;
    ParticleSystem _ParticlesEnabled;

    [NonSerialized] private AudioSource _audioSource;

    // Start is called before the first frame update
    void Start()
    {
        _ParticlesEnabled = this.GetComponent<ParticleSystem>();
    }

    // Update is called once per frame
    void Update()
    {
        timeTillDeath -= Time.deltaTime;
        if (timeTillDeath <= 0)
        { _ParticlesEnabled.Stop(true, ParticleSystemStopBehavior.StopEmitting); }
    }
}
