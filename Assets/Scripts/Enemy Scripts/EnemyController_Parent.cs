using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyController_Parent : MonoBehaviour
{
    // Entity Values ========================
    // Current Vals
    [SerializeField] private EnemyTypes typeOfEnemy = EnemyTypes.BasicEnemy;
    [SerializeField] public EnemyStates currentState = EnemyStates.Standing;
    [SerializeField] public int health;
    [SerializeField] public bool facingRight = false;
    [SerializeField] private bool movingRight = false;
    [SerializeField] private float verticalRangeOfVision = 4;
    [SerializeField] private float horizontalRangeOfVision = 6;
    [NonSerialized] private float originalGravityScale;

    // [SerializeField] private (SpriteVar) currentSprite = (basesprite);

    // Maximum & Static Vals
    [Tooltip("The health the enemy starts with and the maximum the enemy's health can be.")]
    [SerializeField] private int health_max = 2;
    [SerializeField] private float walkingSpeed = 2.5f;
    [SerializeField] private float runningSpeed = 5.0f;
    [SerializeField] private float jumpStrength = 8;
    [SerializeField] private float rollStrengthX = 9;
    [SerializeField] private float rollStrengthY = 2;
    [NonSerialized] private float rollRotationSpeed = 10;
    [NonSerialized] private float rollRotation = 0;
    [SerializeField] public int attackStrength = 1;
    [NonSerialized] private float hurtKnockback = 3;

    // Sprite & Animation Vals
    // stuff here

    // Enemy Boolean Flags
    // 'Limiter' Flags
    [NonSerialized] public bool canJump = true;                         // When false the enemy cannot jump, when true they can.
    [NonSerialized] private bool canMove = true;                        // When false the enemy cannot move, when true they can.
    [NonSerialized] private bool canAttack = true;                      // When false the enemy cannot attack, when true they can.
    [NonSerialized] private bool canRoll = true;
    [NonSerialized] private bool canSearch = true;
    [NonSerialized] public bool hasRollCollision = false;
    [NonSerialized] private bool isPlayerSeen = false;
    [NonSerialized] public bool isHurt = false;
    [NonSerialized] public bool isHurt_FromRight = false;
    [NonSerialized] private bool isHurt_Knockback = false;
    [NonSerialized] private bool jumpPeakReached = false;
    [SerializeField] private bool ledgeAhead = false;
    // 'Button' Flags
    [NonSerialized] private bool startAttacking = false;
    [NonSerialized] private bool doJump = false;

    // Enemy Cooldown Vals
    // Attack Cooldown
    [NonSerialized] private float attackCooldown = 0;
    [SerializeField] private float attackCooldown_length = 2;            // Placeholder Value
    [NonSerialized] private bool attackCooldown_flag = false;
    [NonSerialized] private float attackCooldown2 = 0;
    [SerializeField] private float attackCooldown2_length = 5;            // Placeholder Value
    [NonSerialized] private bool attackCooldown2_flag = false;
    [NonSerialized] private float animationImmunity = 0;
    [NonSerialized] private float turnAround_cooldown = 0;

    // Enemy Types & States
    public enum EnemyTypes
    {
        [Tooltip("An enemy that only can move and stand still. Cannot harm the player.")]
        BasicEnemy,
        [Tooltip("Walks left and right but begins rolling at the player when they spot them.")]
        Roller,
        [Tooltip("Patrols around and chases the player when in sight, projectiles fall off it's body as it moves.")]
        LeakyHunter,
        [Tooltip("Shoots projectiles unaffected by gravity in a straight line, jumps slighty upwards when it does so.")]
        JumpyShooter,
        [Tooltip("Shoots a burst of gravity projectiles with random arcs in the general direction of the player before needing to reload.")]
        BurstMortarEnemy,
        [Tooltip("Shoots three gravity projectiles after eachother before needing to reload.")]
        TripleMortarEnemy,
    }

    public enum EnemyStates
    {
        [Tooltip("The enemy isn't moving or performing any action.")]
        Standing,
        [Tooltip("The enemy is walking across the ground.")]
        Walking,
        [Tooltip("The enemy is running across the ground, like walking but faster.")]
        Running,
        [Tooltip("The enemy is jumping up and down repeatedly.")]
        Jumping,
        [Tooltip("The enemy is performing a close range melee attack.")]
        MeleeAttacking,
        [Tooltip("The enemy is performing a ranged projectile attack.")]
        RangedAttacking,
        [Tooltip("The enemy is performing a rolling attack.")]
        Rolling,
        [Tooltip("The enemy is patrolling the area, a combination of walking, standing and turning around.")]
        Roaming,
        [Tooltip("The enemy is chasing their target until they can't see them for a couple of seconds.")]
        Chasing,
    }
     
    // Enemy SFX
    [NonSerialized] private AudioSource _audioSource;
    [SerializeField] private AudioClip attackingSFX;
    [SerializeField] private AudioClip hurtSFX;
    [SerializeField] private GameObject poofSFXEmitter;
    private bool attackSoundPlayed = false;
    private bool hurtSoundPlayed = false;

    // Death Smoke
    [SerializeField] private GameObject deathSmoke;

    // Cached Projectiles
    [SerializeField] private GameObject leakyHunter_Projectile_1;
    [SerializeField] private GameObject jumpyShooter_Projectile_1;

    // Animator
    Animator _animator;

    // Entity Values ========================

    bool isObjectHere(Vector3 position, string tag)
    {
        Collider[] intersecting = Physics.OverlapSphere(position, 0.0001f);
        if (intersecting.Length == 0)
        {
            return false;
        }
        else
        {
            foreach (var objectInside in intersecting)
            {
                if (objectInside.tag == tag)
                {
                    return true;
                }
            }
            return false;
        }
    }

    bool isObjectHere(Vector3 position, GameObject gameObj)
    {
        Collider[] intersecting = Physics.OverlapSphere(position, 0.0001f);
        if (intersecting.Length == 0)
        {
            return false;
        }
        else
        {
            foreach (var objectInside in intersecting)
            {
                if (objectInside == gameObj)
                {
                    return true;
                }
            }
            return false;
        }
    }

    void TurnAround()
    {
        if (facingRight)
        {
            facingRight = false;
        }
        else
        {
            facingRight = true;
        }

        if (movingRight)
        {
            movingRight = false;
        }
        else
        {
            movingRight = true;
        }
    }

    void Actions()
    {

        switch (typeOfEnemy)
        {
            case EnemyTypes.Roller:
                {
                    // Walking
                    if (currentState == EnemyStates.Walking)
                    {
                        if (movingRight)
                        {
                            _enemyRigidbody2D.velocity = new Vector2(walkingSpeed, _enemyRigidbody2D.velocity.y);
                        }
                        else
                        {
                            _enemyRigidbody2D.velocity = new Vector2(-walkingSpeed, _enemyRigidbody2D.velocity.y);
                        }
                        canRoll = true;
                    }

                    // Rolling
                    if (currentState == EnemyStates.Rolling)
                    {
                        if (canRoll)
                        {
                            if (facingRight)
                            {
                                _enemyRigidbody2D.velocity = new Vector2(rollStrengthX, rollStrengthY);
                            }
                            else
                            {
                                _enemyRigidbody2D.velocity = new Vector2(-rollStrengthX, rollStrengthY);
                            }
                            canRoll = false;
                        }
                        if (_enemyRigidbody2D.velocity.x < 2 && _enemyRigidbody2D.velocity.x > -2)
                        {
                            currentState = EnemyStates.Walking;
                        }
                    }
                    break;
                }

            case EnemyTypes.LeakyHunter:
                {
                    // Roaming
                    if (currentState == EnemyStates.Roaming)
                    {
                        if (movingRight)
                        {
                            _enemyRigidbody2D.velocity = new Vector2(walkingSpeed, _enemyRigidbody2D.velocity.y);
                        }
                        else
                        {
                            _enemyRigidbody2D.velocity = new Vector2(-walkingSpeed, _enemyRigidbody2D.velocity.y);
                        }

                        if (doJump)
                        {
                            doJump = false;
                            if (canJump)
                            {
                                _enemyRigidbody2D.velocity = new Vector2(/*_enemyRigidbody2D.velocity.x*/ 0, jumpStrength);
                                canJump = false;
                                jumpPeakReached = false;
                            }
                        }

                        if (attackCooldown > 0.75f)
                        {
                            attackCooldown /= 2;
                        }
                    }

                    // Chasing
                    if (currentState == EnemyStates.Chasing)
                    {
                        if (movingRight)
                        {
                            _enemyRigidbody2D.velocity = new Vector2(runningSpeed, _enemyRigidbody2D.velocity.y);
                        }
                        else
                        {
                            _enemyRigidbody2D.velocity = new Vector2(-runningSpeed, _enemyRigidbody2D.velocity.y);
                        }
                    }

                    // Pouncing / Melee Attacking
                    if (currentState == EnemyStates.MeleeAttacking && canJump)
                    {
                        attackCooldown2 = attackCooldown2_length;
                        _enemyRigidbody2D.gravityScale = 0.5f;

                        if (facingRight)
                        {
                            _enemyRigidbody2D.velocity = new Vector2(runningSpeed * 13.25f, jumpStrength * 0.25f);
                        }
                        else
                        {
                            _enemyRigidbody2D.velocity = new Vector2(-runningSpeed * 13.25f, jumpStrength * 0.25f);
                        }
                    }
                    else
                    {
                        _enemyRigidbody2D.gravityScale = originalGravityScale;
                    }

                    if (attackCooldown2 <= 0)
                    {
                        attackCooldown2 = 0;
                        currentState = EnemyStates.Roaming;
                        canJump = true;
                    }


                    if (attackCooldown <= 0)
                    {
                        attackCooldown = 0;

                        float randMult = UnityEngine.Random.Range(0.15f, 1.25f);
                        attackCooldown = attackCooldown_length * randMult;

                        //float distance = 1.25f;
                        ////int rotation = 0;
                        //if (!facingRight)
                        //{
                        //    distance *= -1;
                        //}

                        GameObject attackProjectile = Instantiate(leakyHunter_Projectile_1, new Vector3(_enemyTransform.position.x/* + distance*/, _enemyTransform.position.y, _enemyTransform.position.z), Quaternion.Euler(0, 0, 0));
                        attackProjectile.GetComponent<Projectile_Default>().projectileFiringAngle = UnityEngine.Random.Range(-45.0f, 45.0f);
                        attackProjectile.GetComponent<Projectile_Default>().ownerCheck(this.gameObject, facingRight);
                    }
                    attackCooldown -= Time.deltaTime;
                    attackCooldown2 -= Time.deltaTime;

                    break;
                }

            case EnemyTypes.JumpyShooter:
                {
                    // Jumping
                    if (currentState == EnemyStates.Jumping)
                    {
                        if (canJump)
                        {
                            _enemyRigidbody2D.velocity = new Vector2(/*_enemyRigidbody2D.velocity.x*/ 0, jumpStrength);
                            canJump = false;
                            jumpPeakReached = false;
                        }

                        if (!jumpPeakReached && _enemyRigidbody2D.velocity.y <= 0)
                        {
                            jumpPeakReached = true;

                            float distance = 1.25f;
                            //int rotation = 0;
                            if (!facingRight)
                            {
                                distance *= -1;
                            }

                            //Transform attack_pos = jumpyShooter_Projectile_1.transform;
                            //attack_pos.position = new Vector3(_enemyTransform.position.x + distance, _enemyTransform.position.y, _enemyTransform.position.z);

                            GameObject attackProjectile = Instantiate(jumpyShooter_Projectile_1, new Vector3(_enemyTransform.position.x + distance, _enemyTransform.position.y, _enemyTransform.position.z), Quaternion.Euler(0, 0, 0));
                            attackProjectile.GetComponent<Projectile_Default>().ownerCheck(this.gameObject, facingRight);
                        }
                    }

                    break;
                }
        }
    }

    void Search()
    {
        switch (typeOfEnemy)
        {
            case EnemyTypes.Roller:
                {
                    if (facingRight)
                    {
                        if (_playerTransform.position.x <= (_enemyTransform.position.x + horizontalRangeOfVision) && _playerTransform.position.x >= (_enemyTransform.position.x))
                        {
                            if (_playerTransform.position.y <= (_enemyTransform.position.y + verticalRangeOfVision) && _playerTransform.position.y >= (_enemyTransform.position.y - _enemyTransform.localScale.y / 2))
                            {
                                isPlayerSeen = true;
                            }
                            else
                            {
                                isPlayerSeen = false;
                            }
                        }
                        else
                        {
                            isPlayerSeen = false;
                        }
                    }
                    else
                    {
                        if (_playerTransform.position.x >= (_enemyTransform.position.x - horizontalRangeOfVision) && _playerTransform.position.x <= (_enemyTransform.position.x))
                        {
                            if (_playerTransform.position.y <= (_enemyTransform.position.y + verticalRangeOfVision) && _playerTransform.position.y >= (_enemyTransform.position.y - _enemyTransform.localScale.y / 2))
                            {
                                isPlayerSeen = true;
                            }
                            else
                            {
                                isPlayerSeen = false;
                            }
                        }
                        else
                        {
                            isPlayerSeen = false;
                        }
                    }

                    break;
                }

            case EnemyTypes.LeakyHunter:
                {
                    if (facingRight)
                    {
                        if (_playerTransform.position.x <= (_enemyTransform.position.x + horizontalRangeOfVision) && _playerTransform.position.x >= (_enemyTransform.position.x))
                        {
                            if (_playerTransform.position.y <= (_enemyTransform.position.y + verticalRangeOfVision) && _playerTransform.position.y >= (_enemyTransform.position.y - _enemyTransform.localScale.y / 2))
                            {
                                isPlayerSeen = true;
                            }
                            else
                            {
                                isPlayerSeen = false;
                            }
                        }
                        else
                        {
                            isPlayerSeen = false;
                        }
                    }
                    else
                    {
                        if (_playerTransform.position.x >= (_enemyTransform.position.x - horizontalRangeOfVision) && _playerTransform.position.x <= (_enemyTransform.position.x))
                        {
                            if (_playerTransform.position.y <= (_enemyTransform.position.y + verticalRangeOfVision) && _playerTransform.position.y >= (_enemyTransform.position.y - _enemyTransform.localScale.y / 2))
                            {
                                isPlayerSeen = true;
                            }
                            else
                            {
                                isPlayerSeen = false;
                            }
                        }
                        else
                        {
                            isPlayerSeen = false;
                        }
                    }

                    break;
                }

            case EnemyTypes.JumpyShooter:
                {
                    if (facingRight)
                    {
                        if (_playerTransform.position.x <= (_enemyTransform.position.x + horizontalRangeOfVision) && _playerTransform.position.x >= (_enemyTransform.position.x - horizontalRangeOfVision / 3))
                        {
                            if (_playerTransform.position.y <= (_enemyTransform.position.y + verticalRangeOfVision) && _playerTransform.position.y >= (_enemyTransform.position.y - verticalRangeOfVision/3 - _enemyTransform.localScale.y / 2))
                            {
                                isPlayerSeen = true;
                            }
                            else
                            {
                                isPlayerSeen = false;
                            }
                        }
                        else
                        {
                            isPlayerSeen = false;
                        }
                    }
                    else
                    {
                        if (_playerTransform.position.x >= (_enemyTransform.position.x - horizontalRangeOfVision) && _playerTransform.position.x <= (_enemyTransform.position.x + horizontalRangeOfVision / 3))
                        {
                            if (_playerTransform.position.y <= (_enemyTransform.position.y + verticalRangeOfVision) && _playerTransform.position.y >= (_enemyTransform.position.y - verticalRangeOfVision/3 - _enemyTransform.localScale.y / 2))
                            {
                                isPlayerSeen = true;
                            }
                            else
                            {
                                isPlayerSeen = false;
                            }
                        }
                        else
                        {
                            isPlayerSeen = false;
                        }
                    }

                    break;
                }
        }
    }

    void Behaviour()
    {
        switch (typeOfEnemy)
        {
            case EnemyTypes.Roller:
                {
                    // Searching
                    if (currentState == EnemyStates.Walking || currentState == EnemyStates.Standing)
                    {
                        canSearch = true;
                    }
                    else
                    {
                        canSearch = false;
                    }

                    // Rolling
                    if (isPlayerSeen)
                    {
                        if (currentState != EnemyStates.Rolling)
                        {
                            if (_audioSource.isPlaying == false && _audioSource.clip != attackingSFX)
                            {
                                _audioSource.PlayOneShot(attackingSFX, 0.75f);
                            }
                            currentState = EnemyStates.Rolling;
                        }
                    }

                    if (currentState != EnemyStates.Rolling)
                    {
                        hasRollCollision = false;
                    }

                    // Animation
                    if (currentState == EnemyStates.Standing)
                    {
                        if (_animator.GetBool("isStanding") == false)
                        {
                            Instantiate(deathSmoke, new Vector3(_enemyTransform.position.x, _enemyTransform.position.y, _enemyTransform.position.z), Quaternion.Euler(0, 0, 0));
                        }
                        _animator.SetBool("isStanding", true);
                    }
                    else
                    {
                        if (_animator.GetBool("isStanding") == true)
                        {
                            Instantiate(deathSmoke, new Vector3(_enemyTransform.position.x, _enemyTransform.position.y, _enemyTransform.position.z), Quaternion.Euler(0, 0, 0));
                        }
                        _animator.SetBool("isStanding", false);

                        if (currentState == EnemyStates.Walking)
                        {
                            if (_animator.GetBool("isWalking") == false)
                            {
                                Instantiate(deathSmoke, new Vector3(_enemyTransform.position.x, _enemyTransform.position.y, _enemyTransform.position.z), Quaternion.Euler(0, 0, 0));
                            }
                            _animator.SetBool("isWalking", true);
                        }
                        else
                        {
                            if (_animator.GetBool("isWalking") == true)
                            {
                                Instantiate(deathSmoke, new Vector3(_enemyTransform.position.x, _enemyTransform.position.y, _enemyTransform.position.z), Quaternion.Euler(0, 0, 0));
                            }
                            _animator.SetBool("isWalking", false);

                            if (currentState == EnemyStates.Rolling)
                            {
                                if (_animator.GetBool("isRolling") == false)
                                {
                                    Instantiate(deathSmoke, new Vector3(_enemyTransform.position.x, _enemyTransform.position.y, _enemyTransform.position.z), Quaternion.Euler(0, 0, 0));
                                }
                                _animator.SetBool("isRolling", true);
                            }
                            else
                            {
                                if (_animator.GetBool("isRolling") == true)
                                {
                                    Instantiate(deathSmoke, new Vector3(_enemyTransform.position.x, _enemyTransform.position.y, _enemyTransform.position.z), Quaternion.Euler(0, 0, 0));
                                }
                                _animator.SetBool("isRolling", false);
                            }
                        }
                    }

                    break;
                }

            case EnemyTypes.LeakyHunter:
                {
                    // Searching
                    if (currentState == EnemyStates.Standing || currentState == EnemyStates.Roaming)
                    {
                        canSearch = true;
                    }
                    else
                    {
                        canSearch = false;
                    }

                    // Player Detected
                    if (isPlayerSeen && currentState != EnemyStates.MeleeAttacking)
                    {
                        if (currentState != EnemyStates.Chasing)
                        {
                            if (!attackSoundPlayed)
                            {
                                if (_audioSource.isPlaying == false && _audioSource.clip != attackingSFX)
                                {
                                    _audioSource.PlayOneShot(attackingSFX, 0.75f);
                                }
                                attackSoundPlayed = true;
                            }
                            currentState = EnemyStates.Chasing;
                        }
                    }
                    else
                    {
                        //currentState = EnemyStates.Roaming;
                        attackSoundPlayed = false;
                    }

                    // Turning Around Randomly
                    if (currentState == EnemyStates.Roaming || currentState == EnemyStates.Standing)
                    {
                        float randTurn = UnityEngine.Random.Range(0.0f, 100.0f);
                        if (randTurn < 0.025f)
                        {
                            TurnAround();
                        }
                    }

                    // Stop Roaming Randomly
                    if (currentState == EnemyStates.Roaming)
                    {
                        float randTurn = UnityEngine.Random.Range(0.0f, 100.0f);
                        if (randTurn < 0.05f)
                        {
                            currentState = EnemyStates.Standing;
                        }
                    }
                    // Start Roaming Randomly
                    if (currentState == EnemyStates.Standing)
                    {
                        float randTurn = UnityEngine.Random.Range(0.0f, 100.0f);
                        if (randTurn < 0.05f)
                        {
                            currentState = EnemyStates.Roaming;
                        }
                    }

                    // Chasing
                    if (currentState == EnemyStates.Chasing)
                    {
                        if (_playerTransform.position.x < _enemyTransform.position.x)
                        {
                            facingRight = false;
                            movingRight = false;
                        }
                        else
                        {
                            facingRight = true;
                            movingRight = true;
                        }

                        if (_playerTransform.position.y > _enemyTransform.position.y + _enemyTransform.localScale.y/2)
                        {
                            doJump = true;
                        }

                        float distance = MathF.Abs(_playerTransform.position.x -_enemyTransform.position.x);

                        if (distance < 3 && canJump)
                        {
                            currentState = EnemyStates.MeleeAttacking;
                            canJump = false;
                        }
                    }

                    // Animation
                    if (currentState == EnemyStates.Standing)
                    {
                        if (_animator.GetBool("isStanding") == false)
                        {
                            Instantiate(deathSmoke, new Vector3(_enemyTransform.position.x, _enemyTransform.position.y, _enemyTransform.position.z), Quaternion.Euler(0, 0, 0));
                        }
                        _animator.SetBool("isStanding", true);
                    }
                    else
                    {
                        if (_animator.GetBool("isStanding") == true)
                        {
                            Instantiate(deathSmoke, new Vector3(_enemyTransform.position.x, _enemyTransform.position.y, _enemyTransform.position.z), Quaternion.Euler(0, 0, 0));
                        }
                        _animator.SetBool("isStanding", false);

                        if (currentState == EnemyStates.Roaming)
                        {
                            if (_animator.GetBool("isWalking") == false)
                            {
                                Instantiate(deathSmoke, new Vector3(_enemyTransform.position.x, _enemyTransform.position.y, _enemyTransform.position.z), Quaternion.Euler(0, 0, 0));
                            }
                            _animator.SetBool("isWalking", true);
                        }
                        else
                        {
                            if (_animator.GetBool("isWalking") == true)
                            {
                                Instantiate(deathSmoke, new Vector3(_enemyTransform.position.x, _enemyTransform.position.y, _enemyTransform.position.z), Quaternion.Euler(0, 0, 0));
                            }
                            _animator.SetBool("isWalking", false);

                            if (currentState == EnemyStates.Chasing)
                            {
                                if (_animator.GetBool("isRunning") == false)
                                {
                                    Instantiate(deathSmoke, new Vector3(_enemyTransform.position.x, _enemyTransform.position.y, _enemyTransform.position.z), Quaternion.Euler(0, 0, 0));
                                }
                                _animator.SetBool("isRunning", true);
                            }
                            else
                            {
                                if (_animator.GetBool("isRunning") == true)
                                {
                                    Instantiate(deathSmoke, new Vector3(_enemyTransform.position.x, _enemyTransform.position.y, _enemyTransform.position.z), Quaternion.Euler(0, 0, 0));
                                }
                                _animator.SetBool("isRunning", false);
                            }
                        }
                    }

                    break;
                }

            case EnemyTypes.JumpyShooter:
                {
                    // Searching
                    if (currentState == EnemyStates.Standing || currentState == EnemyStates.Jumping)
                    {
                        canSearch = true;
                    }
                    else
                    {
                        canSearch = false;
                    }

                    // Jumping
                    if (isPlayerSeen)
                    {
                        if (_playerTransform.position.x > _enemyTransform.position.x)
                        {
                            facingRight = true;
                        }
                        if (_playerTransform.position.x < _enemyTransform.position.x)
                        {
                            facingRight = false;
                        }

                        if (facingRight)
                        {
                            if (_playerTransform.position.x <= (_enemyTransform.position.x + horizontalRangeOfVision/2) && _playerTransform.position.x >= (_enemyTransform.position.x - (horizontalRangeOfVision / 3) / 2))
                            {
                                currentState = EnemyStates.Jumping;
                            }
                        }
                        else
                        {
                            if (_playerTransform.position.x >= (_enemyTransform.position.x - horizontalRangeOfVision/2) && _playerTransform.position.x <= (_enemyTransform.position.x + (horizontalRangeOfVision / 3) / 2))
                            {
                                currentState = EnemyStates.Jumping;
                            }
                        }
                    }
                    else
                    {
                        currentState = EnemyStates.Standing;
                    }

                    // Turning Around Randomly
                    if (currentState == EnemyStates.Standing)
                    {
                        float randTurn = UnityEngine.Random.Range(0.0f, 100.0f);
                        if (randTurn < 0.035f)
                        {
                            TurnAround();
                        }
                    }

                    // Animation
                    if (isPlayerSeen)
                    {
                        if (_animator.GetBool("isSeeingPlayer") == false)
                        {
                            Instantiate(deathSmoke, new Vector3(_enemyTransform.position.x, _enemyTransform.position.y, _enemyTransform.position.z), Quaternion.Euler(0, 0, 0));
                        }
                        _animator.SetBool("isSeeingPlayer", true);

                        if ((currentState == EnemyStates.Jumping || isHurt) && animationImmunity == 0)
                        {
                            _animator.SetBool("isJumping", true);
                        }
                        else
                        {
                            _animator.SetBool("isJumping", false);
                        }
                    }
                    else
                    {
                        if (_animator.GetBool("isSeeingPlayer") == true)
                        {
                            Instantiate(deathSmoke, new Vector3(_enemyTransform.position.x, _enemyTransform.position.y, _enemyTransform.position.z), Quaternion.Euler(0, 0, 0));
                        }
                        _animator.SetBool("isSeeingPlayer", false);
                    }

                    if (animationImmunity > 0)
                    { 
                        animationImmunity -= Time.deltaTime; 
                    }
                    else
                    {
                        animationImmunity = 0;
                    }


                    break;
                }
        }

        this.GetComponent<SpriteRenderer>().flipX = facingRight;
    }

private void StayOnLedge()
    {
        //Turn around before ledge
        if (currentState == EnemyStates.Walking || currentState == EnemyStates.Roaming)
        {
            int mult = 1;
            if (!facingRight)
            {
                mult = -1;
            }


                if (!isObjectHere(_enemyTransform.position + new Vector3(0.025f * mult, -0.05f - _enemyTransform.localScale.y / 2, 0), "Geometry"))
                {
                    TurnAround();
                }


        }
    }

    // Cached Components ====================
    private Transform _enemyTransform;
    [NonSerialized] public Rigidbody2D _enemyRigidbody2D;
    private GameObject _player;
    private Transform _playerTransform;
    //private Camera mainCamera;
    //private GameController _gameController;
    // Cached Components ====================
    void Start()
    {
        health = health_max;
        _enemyTransform = transform;
        _enemyRigidbody2D = GetComponent<Rigidbody2D>();
        _player = GameObject.Find("Player");
        _playerTransform = _player.transform;
        _audioSource = GetComponent<AudioSource>();
        _audioSource.volume = 0.65f;

        if (typeOfEnemy == EnemyTypes.JumpyShooter || typeOfEnemy == EnemyTypes.Roller || typeOfEnemy == EnemyTypes.LeakyHunter)
        {
            _animator = GetComponent<Animator>();
        }
        originalGravityScale = _enemyRigidbody2D.gravityScale;
    }

    // Update is called once per frame
    void Update()
    {
        if (health <= 0)
        {
            Instantiate(poofSFXEmitter, new Vector3(_enemyTransform.position.x, _enemyTransform.position.y, _enemyTransform.position.z), Quaternion.Euler(0, 0, 0));
            Instantiate(deathSmoke, new Vector3(_enemyTransform.position.x, _enemyTransform.position.y, _enemyTransform.position.z), Quaternion.Euler(0, 0, 0));
            Destroy(gameObject);
        }

        if (turnAround_cooldown > 0)
        {
            turnAround_cooldown -= Time.deltaTime;
        }
        if (turnAround_cooldown < 0)
        {
            turnAround_cooldown = 0;
        }

        if (!isHurt)
        {
            hurtSoundPlayed = false;
            isHurt_Knockback = false;

            Behaviour();
            if (canSearch)
            {
                Search();
            }
            Actions();

            if (currentState != EnemyStates.Rolling)
            {
                _enemyTransform.rotation = Quaternion.Euler(0, 0, 0);
                rollRotation = 0;
            }
            else
            {

                if (facingRight)
                {
                    rollRotation += rollRotationSpeed;
                }
                else
                {
                    rollRotation -= rollRotationSpeed;
                }
                _enemyTransform.rotation = Quaternion.Euler(0, 0, rollRotation);
            }
        }
        else
        {
            if (!hurtSoundPlayed)
            {
                hurtSoundPlayed = true;
                _audioSource.PlayOneShot(hurtSFX);
            }

            _enemyTransform.rotation = Quaternion.Euler(0, 0, 0);

            if (!isHurt_Knockback)
            {
                isHurt_Knockback = true;
                if (isHurt_FromRight)
                {
                    _enemyRigidbody2D.velocity = new Vector2(-hurtKnockback * 1.75f, hurtKnockback);
                }
                else
                {
                    _enemyRigidbody2D.velocity = new Vector2(hurtKnockback * 1.75f, hurtKnockback);
                }

                if (_playerTransform.position.x < _enemyTransform.position.x)
                {
                    facingRight = false;
                    movingRight = false;
                }
                else
                {
                    facingRight = true;
                    movingRight = true;
                }
            }

            if (_enemyRigidbody2D.velocity.x < 2 && _enemyRigidbody2D.velocity.x > -2)
            {
                isHurt = false;
            }
        }
    }

    private void OnTriggerEnter2D(Collider2D other)
    {

        if (other.gameObject.tag == "Geometry" || other.gameObject.tag == "One-Way Geometry")
        {
            if (other.gameObject.GetComponent<Rigidbody2D>().transform.position.y < this.gameObject.GetComponent<Rigidbody2D>().transform.position.y)
            {
                canJump = true;

                if (typeOfEnemy == EnemyTypes.JumpyShooter)
                {
                    _animator.SetBool("isJumping", false);
                    animationImmunity = 0.0025f;
                }
            }
        }

        if (other.gameObject.tag == "Geometry")
        {
            if (currentState == EnemyStates.Walking || currentState == EnemyStates.Roaming)
            {
                // Walking into Wall
                if (other.gameObject.GetComponent<Rigidbody2D>().transform.position.y > this.gameObject.GetComponent<Rigidbody2D>().transform.position.y)
                {

                    if (other.gameObject.GetComponent<Rigidbody2D>().transform.position.x < this.gameObject.GetComponent<Rigidbody2D>().transform.position.x)
                    {
                        facingRight = true;
                        movingRight = true;
                    }
                    else
                    {
                        facingRight = false;
                        movingRight = false;
                    }
                }
            }

            if (currentState == EnemyStates.Rolling)
            {
                if (other.gameObject.GetComponent<Rigidbody2D>().transform.position.y > this.gameObject.GetComponent<Rigidbody2D>().transform.position.y)
                {
                    if (!hasRollCollision)
                    {
                        hasRollCollision = true;
                        _enemyRigidbody2D.velocity = new Vector2(-_enemyRigidbody2D.velocity.x/1.5f, _enemyRigidbody2D.velocity.y);
                    }
                }
            }
        }


    }

    private void OnCollisionExit2D(Collision2D other)
    {
        if (other.gameObject.tag == "Geometry")
        {
            if (currentState == EnemyStates.Walking || currentState == EnemyStates.Roaming)
            {
                if (other.gameObject.GetComponent<Rigidbody2D>().transform.position.y <= _enemyTransform.position.y /*- _enemyTransform.localScale.y/2*/)
                {
                    if (turnAround_cooldown <= 0)
                    {
                        TurnAround();
                        turnAround_cooldown = 0.525f;


                        _enemyTransform.position += new Vector3(0, 0.2f, 0);
                        if (facingRight)
                        {
                            _enemyTransform.position += new Vector3(0.55f, 0, 0);
                        }
                        else
                        {
                            _enemyTransform.position += new Vector3(-0.55f, 0, 0);
                        }
                    }
                }
            }
        }

        if (other.gameObject.tag == "One-Way Geometry")
        {
            if (currentState == EnemyStates.Walking || currentState == EnemyStates.Roaming)
            {
                if (other.gameObject.GetComponent<Rigidbody2D>().transform.position.y <= _enemyTransform.position.y /*- _enemyTransform.localScale.y/2*/)
                {
                    if (turnAround_cooldown <= 0)
                    {
                        TurnAround();
                        turnAround_cooldown = 0.75f;


                        _enemyTransform.position += new Vector3(0, 0.2f, 0);
                        if (facingRight)
                        {
                            _enemyTransform.position += new Vector3(0.55f, 0, 0);
                        }
                        else
                        {
                            _enemyTransform.position += new Vector3(-0.55f, 0, 0);
                        }
                    }
                }
            }
        }
    }

    //private void OnTriggerStay2D(Collider2D other)
    //{
    //    if (other.gameObject.tag == "Geometry" || other.gameObject.tag == "One-Way Geometry")
    //    {
    //        if (other.gameObject.GetComponent<Rigidbody2D>().transform.position.y <= this.gameObject.GetComponent<Rigidbody2D>().transform.position.y - _enemyTransform.localScale.y / 2)
    //        {
    //            // Turn around before ledge
    //            if (currentState == EnemyStates.Walking || currentState == EnemyStates.Roaming)
    //            {
    //                int mult;
    //                if (!facingRight)
    //                {
    //                    mult = -1;
    //                }
    //                else
    //                {
    //                    mult = 1;
    //                }

    //                if (other.gameObject.tag == "Geometry")
    //                {
    //                    if (isObjectHere(_enemyTransform.position + new Vector3(0.025f * mult, -0.15f - _enemyTransform.localScale.y / 2, 0), other.gameObject) == false)
    //                    //if (!Physics2D.OverlapPoint(new Vector2(_enemyTransform.position.x + (0.025f * mult), _enemyTransform.position.y - (_enemyTransform.localScale.y/2)), LayerMask.NameToLayer("Geometry")))
    //                    {
    //                        if (mult == -1)
    //                        {
    //                            facingRight = true;
    //                            movingRight = true;
    //                        }
    //                        else
    //                        {
    //                            facingRight = false;
    //                            movingRight = false;
    //                        }

    //                        ledgeAhead = true;
    //                    }
    //                    else
    //                    {
    //                        ledgeAhead = false;
    //                    }
    //                }
    //                if (other.gameObject.tag == "One-Way Geometry")
    //                {
    //                    if (isObjectHere(_enemyTransform.position + new Vector3(0.025f * mult, -0.15f - _enemyTransform.localScale.y / 2, 0), other.gameObject) == false)
    //                    {
    //                        if (mult == -1)
    //                        {
    //                            facingRight = true;
    //                            movingRight = true;
    //                        }
    //                        else
    //                        {
    //                            facingRight = false;
    //                            movingRight = false;
    //                        }

    //                        ledgeAhead = true;
    //                    }
    //                    else
    //                    {
    //                        ledgeAhead = false;
    //                    }
    //                }
    //            }
    //        }
    //    }
    //}

    private void OnCollisionEnter2D(Collision2D other)
    {
        if (other.gameObject.CompareTag("Player"))
        {
            if (currentState == EnemyStates.Rolling)
            {
                if (!hasRollCollision)
                {
                    _player.GetComponent<PlayerController>().isHurt = true;
                    if (this.transform.position.x <= _player.transform.position.x)
                    {
                        _player.GetComponent<PlayerController>().isHurt_FromRight = false;
                    }
                    else
                    {
                        _player.GetComponent<PlayerController>().isHurt_FromRight = true;
                    }
                    _player.GetComponent<PlayerController>().health -= attackStrength;
                    hasRollCollision = true;
                    _enemyRigidbody2D.velocity = new Vector2(-_enemyRigidbody2D.velocity.x / 2, _enemyRigidbody2D.velocity.y);
                }
            }
            if (currentState != EnemyStates.Rolling)
            {
                _player.GetComponent<PlayerController>().isHurt = true;
                if (this.transform.position.x <= _player.transform.position.x)
                {
                    _player.GetComponent<PlayerController>().isHurt_FromRight = false;
                }
                else
                {
                    _player.GetComponent<PlayerController>().isHurt_FromRight = true;
                }
                _player.GetComponent<PlayerController>().health -= 1;
                _enemyRigidbody2D.velocity = new Vector2(-_enemyRigidbody2D.velocity.x / 2, _enemyRigidbody2D.velocity.y);
            }
        }
    }

}
